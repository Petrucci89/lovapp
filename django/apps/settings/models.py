from django.db import models
from solo.models import SingletonModel
from django.core.validators import MaxValueValidator, MinValueValidator
from django.core.exceptions import ValidationError
from ckeditor_uploader.fields import RichTextUploadingField

# Create your models here.

class ConfigSettings(SingletonModel):

    text_for_app = RichTextUploadingField()

    def __str__(self):
        return "Настройки"

    class Meta:
        verbose_name = "Настройки"


