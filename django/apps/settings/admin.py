from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import ConfigSettings

@admin.register(ConfigSettings)
class ConfigSettingsAdmin(SingletonModelAdmin):
    pass
