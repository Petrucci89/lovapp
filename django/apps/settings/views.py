from rest_framework.views import APIView
from rest_framework.response import Response
from .models import ConfigSettings


class ConfigView(APIView):
    def get(self, request, *args, **kwargs):

        config = ConfigSettings.get_solo()

        response_data = {
            "text_for_app": config.text_for_app
        }

        return Response(response_data)
