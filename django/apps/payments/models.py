from django.db import models
from django.utils import timezone
from django.conf import settings
from apps.users.models import User
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import uuid
from django.conf import settings
from .functions import md5, sha256, to_base64
import requests
import json
# Create your models here.
from django.contrib.auth import get_user_model
User = get_user_model()

from yookassa import Payment
from yookassa import Configuration
Configuration.account_id = settings.YOOKASSA_SHOP_ID
Configuration.secret_key = settings.YOOKASSA_SECRET

class Card(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='cards')
    card_number = models.CharField(max_length=30)
    is_active = models.BooleanField(default=False)
    bill_number = models.CharField(max_length=30, null=True, blank=True)
    yookassa_token = models.CharField(max_length=100, null=True, blank=True)
    parent_order_id = models.UUIDField()
    expiry_month = models.CharField(max_length=2, null=True, blank=True)
    expiry_year = models.CharField(max_length=4, null=True, blank=True)
    card_type = models.CharField(max_length=100, null=True, blank=True)
    issuer_country = models.CharField(max_length=10, null=True, blank=True)
    issuer_name = models.CharField(max_length=100, null=True, blank=True)
    created_at = models.DateTimeField(default=timezone.now)

    # reccurent_payment_attempt = models.PositiveSmallIntegerField(default=0)
    # when_next_reccurent_payment_attempt = models.DateTimeField(default=timezone.now)

    def reccurent_payment(self, amount):

        if not self.is_active:
            return

        price = amount

        recurrent_payment = RecurrentPayment.objects.create(card=self, amount=price)

        PAYMENT_METHOD = "YOOKASSA"

        # UNITELLER
        # if self.user.reg_platform in ['ios', 'android']:
        if PAYMENT_METHOD == 'UNITELLER':

            Shop_IDP = settings.UNITELLER_SHOP_ID
            Password = settings.UNITELLER_PASSWORD

            price = f"{price:.2f}"

            Order_IDP = recurrent_payment.order_id
            Parent_Order_IDP = self.parent_order_id
            Subtotal_P = price

            receipt_json = {
                "customer": {
                    "email": self.user.email,
                    "id": self.id
                },
                "lines": [
                    {
                        'name': "lovapp.ru",
                        'price': price,
                        'qty': 1,
                        'sum': price,
                        'taxmode': 1,
                        'vat': -1,
                        'payattr': 1,
                        'lineattr': 4,
                    },
                ],
                "params": {
                    "place": "https://lovapp.ru"
                },
                "total": price
            }

            Receipt = to_base64(json.dumps(receipt_json))

            Signature = md5(md5(Shop_IDP) + '&' + md5(Order_IDP) + '&' + md5(Subtotal_P) + '&' + md5(Parent_Order_IDP) + '&' + md5(Password))
            Signature = Signature.upper()

            ReceiptSignature = sha256(sha256(Shop_IDP) + '&' + sha256(Order_IDP) + '&' + sha256(Subtotal_P) + '&' + sha256(Receipt) + '&' + sha256(Password))
            ReceiptSignature = ReceiptSignature.upper()

            reccurent_payment_url = f'https://fpay.uniteller.ru/v1/recurrent?' \
                f'Shop_IDP={Shop_IDP}&' \
                f'Order_IDP={Order_IDP}&' \
                f'Subtotal_P={Subtotal_P}&' \
                f'Parent_Order_IDP={Parent_Order_IDP }&' \
                f'Signature={Signature}&' \
                f'Receipt={Receipt}&' \
                f'ReceiptSignature={ReceiptSignature}'

            result = requests.get(reccurent_payment_url).text

            is_paid = False

            if 'AS000' in result:
                is_paid = True

            recurrent_payment.is_paid = is_paid
            recurrent_payment.request_log = result
            recurrent_payment.save(update_fields=['is_paid', 'request_log'])

            return recurrent_payment.is_paid

        # YOOKASSA
        elif PAYMENT_METHOD == 'YOOKASSA':

            is_paid = False

            try:

                payment = Payment.create({
                    "amount": {
                        "value": f"{amount:.2f}",
                        "currency": "RUB"
                    },
                    "payment_method_id": self.yookassa_token,
                    "description": f"Recurrent payment {recurrent_payment.id}"
                })

                # {
                #   "id": "255350c9-000f-5000-a000-1f211b3ea0a7",
                #   "status": "succeeded",
                #   "paid": true,
                #   "amount": {
                #     "value": "2.00",
                #     "currency": "RUB"
                #   },
                #   "authorization_details": {
                #     "rrn": "10000000000",
                #     "auth_code": "000000"
                #   },
                #   "captured_at": "2018-07-18T17:20:50.825Z",
                #   "created_at": "2018-07-18T17:18:39.345Z",
                #   "description": "Заказ №72",
                #   "metadata": {},
                #   "payment_method": {
                #     "type": "bank_card",
                #     "id": "22e18a2f-000f-5000-a000-1db6312b7767",
                #     "saved": true,
                #     "card": {
                #       "first6": "555555",
                #       "last4": "4444",
                #       "expiry_month": "07",
                #       "expiry_year": "2022",
                #       "card_type": "MasterCard",
                #       "issuer_country": "RU",
                #       "issuer_name": "Sberbank"
                #     },
                #     "title": "Bank card *4444"
                #   },
                #   "refundable": true,
                #   "refunded_amount": {
                #     "value": "0.00",
                #     "currency": "RUB"
                #   },
                #   "recipient": {
                #     "account_id": "100001",
                #     "gateway_id": "1000001"
                #   },
                #   "test": false
                # }

                # print("yookassa_reccurent_payment", payment.json())

            except Exception as e:

                recurrent_payment.request_log = str(e)
                recurrent_payment.save(update_fields=['request_log'])
                return is_paid

            else:

                recurrent_payment.request_log = json.dumps(payment.json())
                recurrent_payment.order_id = payment.id
                recurrent_payment.save(update_fields=['order_id', 'request_log'])

                if payment.status != 'waiting_for_capture':
                    return recurrent_payment.is_paid

                try:
                    payment = Payment.capture(payment.id)
                except Exception as e:
                    recurrent_payment.request_log += '\n\nError capture: ' + str(e)
                    recurrent_payment.save(update_fields=['request_log'])
                    return recurrent_payment.is_paid
                else:
                    if payment.status == 'succeeded':
                        is_paid = True
                    recurrent_payment.is_paid = is_paid
                    recurrent_payment.request_log += '\n\n' + json.dumps(payment.json())
                    recurrent_payment.save(update_fields=['is_paid', 'request_log'])
                    return recurrent_payment.is_paid

                # print("yookassa_reccurent_payment capture", payment.json())


    def __str__(self):
        return f'{self.card_number}'

    class Meta:
        verbose_name = 'Банковская карта'
        verbose_name_plural = 'Банковские карты'

# Объединить в один метод UNITELLER и YOOKASSA get_yookassa_payment_url cancel_yookassa_preauth_payment
class PreauthPayment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    uuid4_id = models.UUIDField(default=uuid.uuid4, editable=True)
    order_id = models.UUIDField(default=uuid.uuid4, editable=True)
    amount = models.FloatField()
    is_paid = models.BooleanField("Оплачена?", default=False)
    when_cancel = models.DateTimeField(null=True, blank=True)
    is_canceled = models.BooleanField("Отменена?", default=False)
    when_canceled = models.DateTimeField(null=True, blank=True)
    cancel_log = models.TextField(blank=True, null=True)
    when_capture = models.DateTimeField(null=True, blank=True)
    when_captured = models.DateTimeField(null=True, blank=True)
    capture_log = models.TextField(blank=True, null=True)
    is_captured = models.BooleanField("Captured?", default=False)
    created_at = models.DateTimeField("Дата", auto_now_add=True, auto_now=False)

    class Meta:
        verbose_name = "Преавторизационный Платеж"
        verbose_name_plural = "Преавторизационные Платежи"

    def get_parent_payment_url(self, email):

        Shop_IDP = settings.UNITELLER_SHOP_ID
        Order_IDP = self.order_id
        Subtotal_P = "1.00"
        MeanType = ""
        EMoneyType = ""
        Lifetime = ""
        Customer_IDP = ""
        Card_IDP = ""
        IData = ""
        PT_Code = ""
        password = settings.UNITELLER_PASSWORD

        Signature = md5(md5(Shop_IDP) + '&' + md5(Order_IDP) + '&' + md5(Subtotal_P) + '&' + md5(MeanType) + '&' + md5(EMoneyType) + '&' + md5(Lifetime) + '&' + md5(Customer_IDP) + '&' + md5(Card_IDP) + '&' + md5(IData) + '&' + md5(PT_Code) + '&' + md5(password))
        Signature = Signature.upper()

        url = f'https://wpay.uniteller.ru/pay?' \
            f'Shop_IDP={Shop_IDP}&' \
            f'Order_IDP={Order_IDP}&' \
            f'Subtotal_P={Subtotal_P}&' \
            f'Signature={Signature}&' \
            f'URL_RETURN=https://lovapp.ru/&' \
            f'Currency=RUB&' \
            f'Preauth=1&' \
            f'IsRecurrentStart=1&' \
            f'CallbackFields=BillNumber CardNumber&' \
            f'Email={email}'

        return url

    def get_yookassa_payment_url(self):

        payment = Payment.create({
            "amount": {
                "value": "1.00",
                "currency": "RUB"
            },
            "payment_method_data": {
                "type": "bank_card"
            },
            "confirmation": {
                "type": "redirect",
                "return_url": "https://loveapp.ru"
            },
            "description": f"Order {self.pk}",
            "capture": False,
            "save_payment_method": True
        })

        # print("get_yookassa_payment_url", payment.json())
        # print(payment.id)

        self.order_id = payment.id
        self.save(update_fields=['order_id'])

        return payment.confirmation.confirmation_url

    def get_yookassa_payment_url2(self, token):

        # print("token", token)

        payment = Payment.create({
            "payment_token": token,
            "amount": {
                "value": "1.00",
                "currency": "RUB"
            },
            # "payment_method_data": {
            #     "type": "bank_card"
            # },
            "confirmation": {
                "type": "redirect",
                "return_url": f"https://lovapp.ru/payment-result?id={self.uuid4_id}"
            },
            "description": f"Order {self.pk}",
            "capture": False,
            "save_payment_method": True
        })

        # print("get_yookassa_payment_url2", payment.json())
        # print(payment.id)

        self.order_id = payment.id
        self.save(update_fields=['order_id'])

        return payment.json()

    def get_yookassa_confirmation_token(self):

        # print("token", token)

        payment = Payment.create({
            "amount": {
                "value": "1.00",
                "currency": "RUB"
            },
            # "payment_method_data": {
            #     "type": "bank_card"
            # },
            "confirmation": {
                "type": "embedded"
            },
            "description": f"Order {self.pk}",
            "capture": False,
            "save_payment_method": True
        })

        # print("get_yookassa_payment_url2", payment.json())
        # print(payment.id)

        self.order_id = payment.id
        self.save(update_fields=['order_id'])

        return payment.json()

    def cancel_preauth_payment(self, bill_number):

        Shop_ID = settings.UNITELLER_SHOP_ID
        Login = settings.UNITELLER_LOGIN
        Password = settings.UNITELLER_PASSWORD

        cancel_url = f'https://wpay.uniteller.ru/unblock?' \
            f'Billnumber={bill_number}&' \
            f'Shop_ID={Shop_ID}&' \
            f'Login={Login}&' \
            f'Password={Password}'

        r = requests.get(cancel_url)

        self.cancel_log = r.text
        self.save(update_fields=['cancel_log'])

    def cancel_yookassa_preauth_payment(self):

        idempotence_key = str(uuid.uuid4())

        try:

            payment = Payment.cancel(str(self.order_id), idempotence_key)

            # print("cancel_yookassa_preauth_payment", payment.json())
            # preauth_payment.is_canceled = True
            # preauth_payment.save()

        except Exception as e:

            self.when_cancel = None
            self.cancel_log = str(e)
            self.save(update_fields=['when_cancel', 'cancel_log'])

        else:

            self.when_cancel = None
            self.when_canceled = timezone.now()
            self.cancel_log = json.dumps(payment.json())
            if payment.status == 'canceled':
                self.is_canceled = True
            self.save(update_fields=['cancel_log', 'is_canceled', 'when_cancel', 'when_canceled'])

    def capture_yookassa_preauth_payment(self):

        idempotence_key = str(uuid.uuid4())

        try:

            # data = {
            #            "amount": {
            #                "value": "1.00",
            #                "currency": "RUB"
            #            }
            #        }
            #
            # payment = Payment.capture(str(self.order_id), data, idempotence_key)

            payment = Payment.capture(payment_id=str(self.order_id), idempotency_key=idempotence_key)

            # print("cancel_yookassa_preauth_payment", payment.json())
            # preauth_payment.is_canceled = True
            # preauth_payment.save()

        except Exception as e:

            self.when_capture = None
            self.capture_log = str(e)
            self.save(update_fields=['capture_log', 'when_capture'])

        else:

            self.when_capture = None
            self.when_captured = timezone.now()
            self.capture_log = json.dumps(payment.json())
            if payment.status == 'succeeded':
                self.is_captured = True
            self.save(update_fields=['capture_log', 'when_capture', 'is_captured', 'when_captured'])

    def __str__(self):
        return '{} '.format(self.order_id)


class RecurrentPayment(models.Model):

    card = models.ForeignKey(Card, on_delete=models.CASCADE)
    # order_id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    order_id = models.UUIDField(default=uuid.uuid4, editable=True)
    amount = models.FloatField()
    is_paid = models.BooleanField("Оплачена?", default=False)
    request_log = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField("Дата", auto_now_add=True, auto_now=False)

    class Meta:
        verbose_name = "Рекурентный Платеж"
        verbose_name_plural = "Рекурентные Платежи"

    def __str__(self):
        return f'{self.card.card_number}'
