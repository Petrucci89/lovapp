import yookassa
from yookassa import Configuration

# Configuration.configure('<Идентификатор магазина>', '<Секретный ключ>')

Configuration.account_id = '791143'
Configuration.secret_key = 'test_5Mj9NOF34eHFPnQ64ybc6oQhtt2bJHuTxjQPmiagv9s'


from yookassa import Payment

payment = Payment.create({
    "amount": {
        "value": "1.00",
        "currency": "RUB"
    },
    "payment_method_data": {
        "type": "bank_card"
    },
    "confirmation": {
        "type": "redirect",
        "return_url": "https://loveapp.ru"
    },
    "description": "Заказ №1",
    "save_payment_method": True
})

print(payment.json())

print(type(payment.json()))


print(payment.confirmation.confirmation_url)


# {
# 	"id": "27f05b8f-000f-5000-8000-15552af20985",
# 	"status": "pending",
# 	"paid": false,
# 	"amount": {
# 		"value": "1.00",
# 		"currency": "RUB"
# 	},
# 	"confirmation": {
# 		"type": "redirect",
# 		"return_url": "https://loveapp.ru",
# 		"confirmation_url": "https://yoomoney.ru/payments/external/confirmation?orderId=27f05b8f-000f-5000-8000-15552af20985"
# 	},
# 	"created_at": "2021-03-26T20:45:03.892Z",
# 	"description": "ÐÐ°ÐºÐ°Ð· â1",
# 	"metadata": {},
# 	"payment_method": {
# 		"type": "bank_card",
# 		"id": "27f05b8f-000f-5000-8000-15552af20985",
# 		"saved": false
# 	},
# 	"recipient": {
# 		"account_id": "791143",
# 		"gateway_id": "1832127"
# 	},
# 	"refundable": false,
# 	"test": true
# }


from yookassa import Payment

payment = Payment.create({
    "amount": {
        "value": "2.00",
        "currency": "RUB"
    },
    "payment_method_id": "<Идентификатор сохраненного способа оплаты>",
    "description": "Заказ №105"
})



# {
#   "id": "22e18a2f-000f-5000-a000-1db6312b7767",
#   "status": "succeeded",
#   "paid": true,
#   "amount": {
#     "value": "2.00",
#     "currency": "RUB"
#   },
#   "authorization_details": {
#     "rrn": "10000000000",
#     "auth_code": "000000"
#   },
#   "captured_at": "2018-07-18T17:20:50.825Z",
#   "created_at": "2018-07-18T17:18:39.345Z",
#   "description": "Заказ №72",
#   "metadata": {},
#   "payment_method": {
#     "type": "bank_card",
#     "id": "22e18a2f-000f-5000-a000-1db6312b7767",
#     "saved": true,
#     "card": {
#       "first6": "555555",
#       "last4": "4444",
#       "expiry_month": "07",
#       "expiry_year": "2022",
#       "card_type": "MasterCard",
#       "issuer_country": "RU",
#       "issuer_name": "Sberbank"
#     },
#     "title": "Bank card *4444"
#   },
#   "refundable": true,
#   "refunded_amount": {
#     "value": "0.00",
#     "currency": "RUB"
#   },
#   "recipient": {
#     "account_id": "100001",
#     "gateway_id": "1000001"
#   },
#   "test": false
# }
