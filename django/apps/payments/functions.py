import base64
import hashlib


def md5(s):
    m = hashlib.md5()
    m.update(str(s).encode('utf8'))
    return m.hexdigest()


def sha256(s):
    m = hashlib.sha256()
    m.update(str(s).encode('utf8'))
    return m.hexdigest()

def to_base64(s):

    # Standard Base64 Encoding
    encodedBytes = base64.b64encode(str(s).encode("utf-8"))
    encodedStr = str(encodedBytes, "utf-8")

    return encodedStr
