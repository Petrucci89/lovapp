from rest_framework import serializers
from .models import *
from django.contrib.auth import get_user_model
User = get_user_model()


class CardSerializer(serializers.ModelSerializer):

    class Meta:
        model = Card
        fields = (
            'id',
            'card_number',
            'is_active'
        )


class UpdateCardSerializer(serializers.ModelSerializer):

    class Meta:
        model = Card
        fields = (
            'is_active',
        )



class UserCardsSerializer(serializers.ModelSerializer):

    credit_cards = CardSerializer(many=True)

    class Meta:
        model = User
        fields = (
            'credit_cards',
        )
