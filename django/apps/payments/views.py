from django.shortcuts import render
import json
from .models import Card, RecurrentPayment, PreauthPayment
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate
from rest_framework.permissions import IsAuthenticated
from django.core.mail import send_mail
from django.utils.crypto import get_random_string
from django.conf import settings
from .functions import md5
from django.http import HttpResponse
from .serializers import CardSerializer, UserCardsSerializer, UpdateCardSerializer
from rest_framework.generics import CreateAPIView, DestroyAPIView, UpdateAPIView
from django.utils import timezone
from datetime import datetime, timedelta
from django.contrib.auth import get_user_model
from time import sleep
User = get_user_model()

from yookassa import Payment
from yookassa import Configuration
Configuration.account_id = settings.YOOKASSA_SHOP_ID
Configuration.secret_key = settings.YOOKASSA_SECRET

# Create your views here.

# Create your views here.


class PaymentLink(APIView):

    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):

        user = request.user

        preauth_payment = PreauthPayment.objects.create(user=user, amount=1)

        if user.reg_platform in ['ios', 'android']:
            url = preauth_payment.get_parent_payment_url(user.email)
        else:
            url = preauth_payment.get_yookassa_payment_url()

        return Response({"url": url})

    def post(self, request, *args, **kwargs):

        data = json.loads(request.body)

        token = data['token']

        user = request.user

        preauth_payment = PreauthPayment.objects.create(user=user, amount=1)
        # url = preauth_payment.get_parent_payment_url(user.email)
        payment = preauth_payment.get_yookassa_payment_url2(token)

        return Response(json.loads(payment))


class WidjetConfirmationToken(APIView):

    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        user = request.user

        preauth_payment = PreauthPayment.objects.create(user=user, amount=1)
        payment = preauth_payment.get_yookassa_confirmation_token()

        return Response({"payment": json.loads(payment), "uuid4_id": preauth_payment.uuid4_id})


class AppWidjetConfirmationToken(APIView):

    def post(self, request, *args, **kwargs):

        data = json.loads(request.body)

        user_id = data['user_id']

        user = User.objects.get(id=user_id)

        preauth_payment = PreauthPayment.objects.create(user=user, amount=1)
        payment = preauth_payment.get_yookassa_confirmation_token()

        return Response({"payment": json.loads(payment), "uuid4_id": preauth_payment.uuid4_id})


class AppPaymentLink(APIView):

    def post(self, request, *args, **kwargs):

        data = json.loads(request.body)

        user_id = data['user_id']
        token = data['token']

        user = User.objects.get(id=user_id)

        preauth_payment = PreauthPayment.objects.create(user=user, amount=1)
        # url = preauth_payment.get_parent_payment_url(user.email)
        payment = preauth_payment.get_yookassa_payment_url2(token)

        return Response(json.loads(payment))


class PaymentStatus(APIView):

    def post(self, request, *args, **kwargs):

        data = json.loads(request.body)

        uuid4_id = data['uuid4_id']

        preauth_payment = PreauthPayment.objects.get(uuid4_id=uuid4_id)

        try:
            payment = Payment.find_one(payment_id=str(preauth_payment.order_id))
        except:
            return Response({"status": "not_created"})

        return Response({"status": payment.status})


class CancelSubscription(APIView):

    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        request.user.cancel_subscription()

        return Response(status=status.HTTP_200_OK)


class UpdateCreditCard(UpdateAPIView):
    permission_classes = (IsAuthenticated,)

    parser_classes = (JSONParser,)
    renderer_classes = (JSONRenderer,)

    serializer_class = UpdateCardSerializer

    def get_queryset(self):
        queryset = Card.objects.filter(user=self.request.user)
        return queryset


class UserCards(APIView):

    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):

        user_cards_serializer = UserCardsSerializer(instance=request.user)

        return Response(user_cards_serializer.data)


# class CreateDiscountCard(CreateAPIView):
#     permission_classes = (IsAuthenticated,)
#     serializer_class = DiscountCardSerializer
#
#     def perform_create(self, serializer):
#         serializer.save(user=self.request.user)



class UnitellerStatusView(APIView):
    def post(self, request, *args, **kwargs):

        print(request.POST)

        # {'Signature': ['A670D976B76AC000C8F483B76A262AC3'], 'Order_ID': ['24'], 'Status': ['authorized'], 'BillNumber': ['012320251132']}
        # {'Signature': ['2F0919AB218DCAAC1A8F7B41EE507162'], 'Order_ID': ['24'], 'Status': ['canceled'], 'BillNumber': ['012320251132']}>

        Order_ID = request.POST.get('Order_ID')
        Status = request.POST.get('Status')
        Signature = request.POST.get('Signature')
        BillNumber = request.POST.get('BillNumber') or ''
        CardNumber = request.POST.get('CardNumber') or ''

        password = settings.UNITELLER_PASSWORD

        Signature2 = md5(Order_ID + Status + BillNumber + CardNumber + password)
        Signature2 = Signature2.upper()

        print(Signature, Signature2)

        if Signature == Signature2:

            if Status == 'authorized':

                try:
                    preauth_payment = PreauthPayment.objects.get(order_id=Order_ID)
                except:
                    return HttpResponse("OK")

                user = preauth_payment.user

                card = Card.objects.create(user=user,
                                           card_number=CardNumber,
                                           is_active=True,
                                           bill_number=BillNumber,
                                           parent_order_id=Order_ID)

                # деактивируем все пред активные карты если такие есть
                Card.objects.filter(id__lt=card.id, user=user, is_active=True).update(is_active=False)

                preauth_payment.is_paid = True
                preauth_payment.save()

                # обновляем попытки списания
                user.reccurent_payment_attempt = 0
                user.when_next_reccurent_payment_attempt = timezone.now() + timedelta(days=1)
                user.is_premium = True
                user.is_cancel_subscription = False
                user.save(update_fields=["is_premium",
                                         "reccurent_payment_attempt",
                                         "when_next_reccurent_payment_attempt",
                                         "is_cancel_subscription"])

                # Отменяем преавториз платеж
                preauth_payment.cancel_preauth_payment(BillNumber)

                # if 'AS000' in result:
                #     pass

            elif Status == 'canceled':

                try:
                    preauth_payment = PreauthPayment.objects.get(order_id=Order_ID)
                except:
                    return HttpResponse("OK")

                preauth_payment.is_canceled = True
                preauth_payment.save()

        return HttpResponse("OK")


class YookassaStatusView(APIView):
    def post(self, request, *args, **kwargs):

        data = json.loads(request.body)['object']

        print(data)

        if data['status'] in ['succeeded', 'waiting_for_capture'] and data['payment_method']['saved']:

            order_id = data['id']
            description = data['description']

            if description.startswith('Recurrent payment'):
                # реккурентный платеж
                return HttpResponse("OK")

            if not description.startswith('Order'):
                return HttpResponse("OK")

            o_id = int(description[6:])

            try:
                preauth_payment = PreauthPayment.objects.get(id=o_id)
            except Exception as e:
                # print(11111111, e)
                return HttpResponse("OK")

            user = preauth_payment.user

            yookassa_token = data['payment_method']['id']
            card_number = f"{data['payment_method']['card']['first6']}******{data['payment_method']['card']['last4']}"
            expiry_month = data['payment_method']['card']['expiry_month'] if 'expiry_month' in data['payment_method']['card'] else None
            expiry_year = data['payment_method']['card']['expiry_year'] if 'expiry_year' in data['payment_method']['card'] else None
            card_type = data['payment_method']['card']['card_type'] if 'card_type' in data['payment_method']['card'] else None
            issuer_country = data['payment_method']['card']['issuer_country'] if 'issuer_country' in data['payment_method']['card'] else None
            issuer_name = data['payment_method']['card']['issuer_name'] if 'issuer_name' in data['payment_method']['card'] else None

            card = Card.objects.create(user=user,
                                       card_number=card_number,
                                       is_active=True,
                                       bill_number=None,
                                       yookassa_token=yookassa_token,
                                       parent_order_id=order_id,
                                       expiry_month=expiry_month,
                                       expiry_year=expiry_year,
                                       card_type=card_type,
                                       issuer_country=issuer_country,
                                       issuer_name=issuer_name)

            # деактивируем все пред активные карты если такие есть
            Card.objects.filter(id__lt=card.id, user=user, is_active=True).update(is_active=False)

            preauth_payment.is_paid = True
            # preauth_payment.when_cancel = timezone.now() + timedelta(minutes=15)
            preauth_payment.when_capture = timezone.now() + timedelta(minutes=5)
            preauth_payment.save(update_fields=['is_paid', 'when_capture'])

            # обновляем попытки списания
            user.reccurent_payment_attempt = 0
            user.when_next_reccurent_payment_attempt = timezone.now() + timedelta(days=1)
            user.is_premium = True
            user.is_cancel_subscription = False
            user.is_cancel_enable = True
            user.save(update_fields=["is_premium",
                                     "reccurent_payment_attempt",
                                     "when_next_reccurent_payment_attempt",
                                     "is_cancel_subscription",
                                     "is_cancel_enable"])

            # Отменяем преавториз платеж
            # sleep(3)
            # preauth_payment.cancel_yookassa_preauth_payment(order_id)

        return HttpResponse("OK")
