from django.urls import path
from .views import *
    # , LoginView, UserInfoView, \
    # HoroscopeView, PayView, FCMDeviceView, UnitellerStatusView, \
    # PreauthPaymentUrlView, CancelSubscriptionView
from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = [

    path('get_payment_link/', PaymentLink.as_view()),
    path('payment_status/', PaymentStatus.as_view()),
    path('get_app_payment_link/', AppPaymentLink.as_view()),
    path('widjet_confirmation_token/', WidjetConfirmationToken.as_view()),
    path('app_widjet_confirmation_token/', AppWidjetConfirmationToken.as_view()),
    path('cancel_subscription/', CancelSubscription.as_view()),

    #path('update_credit_card/<int:pk>/', UpdateCreditCard.as_view()),
    #path('list/', UserCards.as_view()),
    #path('add_discount_card/', CreateDiscountCard.as_view()),
    #path('delete_discount_card/<int:pk>/', DeleteDiscountCard.as_view()),

    path('uniteller_status/', UnitellerStatusView.as_view()),
    path('yookassa_status/', YookassaStatusView.as_view()),

]
