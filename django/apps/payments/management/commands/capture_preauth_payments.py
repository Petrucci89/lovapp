from django.core.management.base import BaseCommand
from datetime import datetime
from django.contrib.auth import get_user_model
User = get_user_model()
from apps.payments.models import PreauthPayment
from django.utils import timezone

class Command(BaseCommand):
    def handle(self, *args, **options):

        # списываем преавторизационные платежи на 1 р.

        for preauth_payment in PreauthPayment.objects.filter(when_capture__lt=timezone.now(), is_paid=True):

            preauth_payment.capture_yookassa_preauth_payment()

