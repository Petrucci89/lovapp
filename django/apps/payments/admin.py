from django.contrib import admin
from .models import Card, PreauthPayment, RecurrentPayment
# Register your models here.


@admin.register(Card)
class CardAdmin(admin.ModelAdmin):
    list_display = ('user', 'card_number', 'is_active', 'created_at')
    search_fields = ('user__login', 'user__email', 'user__name', )


@admin.register(PreauthPayment)
class PreauthPaymentAdmin(admin.ModelAdmin):
    list_display = ('user', 'order_id', 'uuid4_id', 'is_paid', 'is_canceled', 'created_at',)
    search_fields = ('user__login', 'user__email', 'user__name', 'order_id',)
    list_filter = ('is_paid', 'is_canceled', 'created_at')


@admin.register(RecurrentPayment)
class RecurrentPaymentAdmin(admin.ModelAdmin):
    model = RecurrentPayment
    list_display = ('__str__', 'order_id', 'amount', 'is_paid', 'created_at',)
    search_fields = ('card__user__phone', 'card__user__email', 'card__user__name', 'order_id',)
    list_filter = ('is_paid', 'created_at')
