from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth import get_user_model
from .forms import UserChangeForm, UserCreationForm
from solo.admin import SingletonModelAdmin
from .models import *
User = get_user_model()


class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.

    list_display = ('phone', 'email', 'name', 'is_premium', 'is_bot', 'is_staff', 'is_manager', 'is_block', 'reg_platform', 'device_token', 'created_at', )
    list_filter = ('is_active', 'is_staff', 'is_online', 'is_bot', 'is_manager', 'is_block', 'reg_platform', 'created_at', )
    fieldsets = (
        (None, {'fields': ('phone', 'password')}),
        ('Personal info', {'fields': ('email', 'name', 'birthday', 'city', 'height', 'weight',
                                      'gender', 'search_gender', 'search_purpose', 'education',
                                      'housing', 'appearance', 'children', 'relationship', 'finance',
                                      'alcohol', 'smoking', 'reg_platform', 'is_premium', 'is_online', 'avatar',
                                      'reccurent_payment_attempt', 'when_next_reccurent_payment_attempt', 'is_cancel_subscription', 'is_cancel_enable',
                                      'utm', 'created_at',)}),
        ('Manager/Bot info', {'fields': ('is_manager', 'is_bot', )}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser', 'is_block', 'groups', 'user_permissions',)}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('phone', 'email', 'password1', 'password2'),
        }),
    )
    search_fields = ('phone', 'email', 'name', )
    ordering = ('-id',)
    filter_horizontal = ()


# Now register the new UserAdmin...
admin.site.register(User, UserAdmin)
# ... and, since we're not using Django's built-in permissions,
# unregister the Group model from admin.
# admin.site.unregister(Group)


@admin.register(Country)
class CityAdmin(admin.ModelAdmin):
    list_display = ('name', 'order', )
    list_editable = ('order', )
    ordering = ('order', )


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = ('name', 'country', 'order', )
    list_filter = ('country', )
    list_editable = ('order', )
    ordering = ('order', 'country', )


@admin.register(UserImage)
class UserImageAdmin(admin.ModelAdmin):
    list_display = ('user', 'image', 'created_at', )


@admin.register(Config)
class ConfigAdmin(SingletonModelAdmin):
    pass


@admin.register(Report)
class ReportAdmin(admin.ModelAdmin):
    list_display = ('id', 'reason', 'text', )


# if need to call delete with django action
# class PhotoBlogEntryAdmin(admin.ModelAdmin):
#     actions=['really_delete_selected']
#
#     def get_actions(self, request):
#         actions = super(PhotoBlogEntryAdmin, self).get_actions(request)
#         del actions['delete_selected']
#         return actions
#
#     def really_delete_selected(self, request, queryset):
#         for obj in queryset:
#             obj.delete()
#
#         if queryset.count() == 1:
#             message_bit = "1 photoblog entry was"
#         else:
#             message_bit = "%s photoblog entries were" % queryset.count()
#         self.message_user(request, "%s successfully deleted." % message_bit)
#     really_delete_selected.short_description = "Delete selected entries"
#
# admin.site.register(PhotoBlogEntry, PhotoBlogEntryAdmin)
