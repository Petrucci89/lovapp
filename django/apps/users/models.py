from django.db import models
from django.contrib.auth.models import User, PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
import uuid
from solo.models import SingletonModel
from datetime import datetime, timedelta
from django.utils import timezone
from django.conf import settings
from django.core.mail import send_mail
import requests
import json
import os
from dateutil.relativedelta import relativedelta
from django.utils.timezone import now
from ckeditor_uploader.fields import RichTextUploadingField
from PIL import Image
from io import BytesIO
from django.core.files.base import ContentFile
# Create your models here.


class UserManager(BaseUserManager):

    def _create_user(self, phone, password, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        """
        if not phone:
            raise ValueError('The given username must be set')
        # if 'email' in extra_fields:
        #     extra_fields['email'] = self.normalize_email(extra_fields['email']).lower()
        # phone = phone.replace(' ', '').replace('+', '').replace('(', '').replace(')', '').replace('-', '')
        user = self.model(phone=phone, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, phone, password, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(phone, password, **extra_fields)

    def create_superuser(self, phone, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(phone, password, **extra_fields)

    def with_perm(self, perm, is_active=True, include_superusers=True, backend=None, obj=None):
        if backend is None:
            backends = auth._get_backends(return_tuples=True)
            if len(backends) == 1:
                backend, _ = backends[0]
            else:
                raise ValueError(
                    'You have multiple authentication backends configured and '
                    'therefore must provide the `backend` argument.'
                )
        elif not isinstance(backend, str):
            raise TypeError(
                'backend must be a dotted import path string (got %r).'
                % backend
            )
        else:
            backend = auth.load_backend(backend)
        if hasattr(backend, 'with_perm'):
            return backend.with_perm(
                perm,
                is_active=is_active,
                include_superusers=include_superusers,
                obj=obj,
            )
        return self.none()


class Country(models.Model):

    name = models.CharField(max_length=100)
    order = models.IntegerField(default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Страна'
        verbose_name_plural = 'Страны'


class City(models.Model):

    country = models.ForeignKey(Country, on_delete=models.CASCADE, related_name='cities')
    name = models.CharField(max_length=100)
    order = models.IntegerField(default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'


class User(AbstractBaseUser, PermissionsMixin):

    GENDER = (
        ('M', 'Мужчина'),
        ('F', 'Женщина'),
        ('T', 'Трансгендер')
    )

    SEARCH_GENDER = (
        ('M', 'Мужчину'),
        ('F', 'Женщину'),
        ('T', 'Трансгендера')
    )

    SEARCH_PURPOSE = (
        ('friendship', 'Дружба'),
        ('date', 'Свидания'),
        ('relationship', 'Отношения'),
        ('family', 'Семья'),
        ('travels', 'Путешествия')
    )

    EDUCATION = (
        ('higher', 'Высшее'),
        ('secondary', 'Среднее'),
        ('elementary', 'Начальное'),
        ('academic', 'Академическое'),
        ('incomplete', 'Незаконченное')
    )

    HOUSING = (
        ('have_home', 'Есть жильё'),
        ('rent', 'Снимаю квартиру'),
        ('with_parents', 'Живу с родителями'),
        ('house', 'Живу в доме'),
    )

    APPEARANCE = (
        ('thick', 'Полноватое'),
        ('standart', 'Обычное'),
        ('fit', 'Спортивное'),
        ('thick', 'Худощавое'),
        ('few_extra_kg', 'Пара лишних килограмм'),
    )

    СHILDREN = (
        ('with_me', 'Живут со мной'),
        ('live_separately', 'Живут отдельно'),
        ('dont_want', 'Не хочу детей'),
        ('later', 'Планирую позже'),
    )

    RELATIONSHIP = (
        ('no_relationship', 'Нет отношений'),
        ('married', 'В браке'),
        ('in_relationship', 'В отношениях'),
        ('lost_partner', 'Потерял супруга'),
        ('divorced', 'В разводе'),
    )

    FINANCE = (
        ('looking_for_sponsor', 'Ищу спонсора'),
        ('can_be_sponsor', 'Могу быть спонсором'),
        ('average_income', 'Средний доход'),
    )

    ALCOHOL = (
        ('sometimes', 'Иногда'),
        ('regularly', 'Регулярно'),
        ('never', 'Никогда'),
    )

    SMOKING = (
        ('sometimes', 'Иногда'),
        ('regularly', 'Регулярно'),
        ('never', 'Никогда'),
    )

    PLATFORM = (
        ('web_mob', 'Web Mobile'),
        ('web_desk', 'Web Desktop'),
        ('ios', 'Ios'),
        ('android', 'Android'),
    )

    FRESH_DELTA_DAYS = 7

    phone = models.CharField(max_length=20, unique=True)
    email = models.EmailField(unique=True)
    name = models.CharField(max_length=30, blank=True, null=True)
    birthday = models.DateField(blank=True, null=True)
    city = models.ForeignKey(City, on_delete=models.SET_NULL, blank=True, null=True)
    height = models.PositiveSmallIntegerField(blank=True, null=True)
    weight = models.PositiveSmallIntegerField(blank=True, null=True)
    gender = models.CharField(max_length=1, blank=True, null=True, choices=GENDER)
    search_gender = models.CharField(max_length=40, blank=True, null=True, choices=SEARCH_GENDER)
    search_purpose = models.CharField(max_length=40, blank=True, null=True, choices=SEARCH_PURPOSE)
    education = models.CharField(max_length=40, blank=True, null=True, choices=EDUCATION)
    housing = models.CharField(max_length=40, blank=True, null=True, choices=HOUSING)
    appearance = models.CharField(max_length=40, blank=True, null=True, choices=APPEARANCE)
    children = models.CharField(max_length=40, blank=True, null=True, choices=СHILDREN)
    relationship = models.CharField(max_length=40, blank=True, null=True, choices=RELATIONSHIP)
    finance = models.CharField(max_length=40, blank=True, null=True, choices=FINANCE)
    alcohol = models.CharField(max_length=40, blank=True, null=True, choices=ALCOHOL)
    smoking = models.CharField(max_length=40, blank=True, null=True, choices=SMOKING)
    reg_platform = models.CharField(max_length=20, blank=True, null=True, choices=PLATFORM)

    is_premium = models.BooleanField(default=False)
    is_online = models.BooleanField(default=False)
    avatar = models.ImageField(blank=True, null=True)
    device_token = models.CharField(max_length=255, blank=True, null=True)

    is_cancel_subscription = models.BooleanField(default=False)
    is_cancel_enable = models.BooleanField(default=False)
    reccurent_payment_attempt = models.PositiveSmallIntegerField(default=0)
    when_next_reccurent_payment_attempt = models.DateTimeField(blank=True, null=True)

    is_manager = models.BooleanField(default=False)
    is_bot = models.BooleanField(default=False)

    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    is_block = models.BooleanField(default=False)
    utm = models.JSONField(blank=True, null=True)

    created_at = models.DateTimeField(default=timezone.now)

    objects = UserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'phone'

    @property
    def age(self):
        current = now().date()
        return current.year - self.birthday.year - ((current.month, current.day) < (self.birthday.month, self.birthday.day))

    @property
    def is_fresh(self):
        current = now().date()
        delta = current - self.created_at.date()
        return delta.days > self.FRESH_DELTA_DAYS

    @property
    def card(self):
        return self.cards.filter(is_active=True).first()

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def make_reccurent_payment(self):

        if self.is_cancel_subscription:
            self.reccurent_payment_attempt = 10
            self.is_premium = False
            self.save(update_fields=["reccurent_payment_attempt", "is_premium"])
            return

        card = self.card

        if not card:
            self.reccurent_payment_attempt = 11
            self.is_premium = False
            self.save(update_fields=["reccurent_payment_attempt", "is_premium"])
            return

        now = timezone.now()

        if self.reccurent_payment_attempt == 0:

            # 750
            is_paid = card.reccurent_payment(750)

            if is_paid:
                self.is_premium = True
                self.when_next_reccurent_payment_attempt = now + relativedelta(months=1)
                self.reccurent_payment_attempt = 0

            else:
                self.is_premium = False
                self.when_next_reccurent_payment_attempt = now + timedelta(minutes=5)
                self.reccurent_payment_attempt += 1

        elif self.reccurent_payment_attempt < 8:

            #170
            is_paid = card.reccurent_payment(170)

            if is_paid:
                self.is_premium = True
                self.when_next_reccurent_payment_attempt = now + timedelta(weeks=1)
                self.reccurent_payment_attempt = 0

            else:

                if self.reccurent_payment_attempt == 7:
                    card.is_active = False
                    card.save(update_fields=["is_active"])
                    self.is_cancel_enable = False

                self.is_premium = False
                self.when_next_reccurent_payment_attempt = now + timedelta(days=1)
                self.reccurent_payment_attempt += 1

        self.save(update_fields=["is_premium", "is_cancel_enable",
                                 "when_next_reccurent_payment_attempt",
                                 "reccurent_payment_attempt"])

    def cancel_subscription(self):

        self.is_cancel_subscription = True
        self.is_cancel_enable = False
        self.save(update_fields=["is_cancel_subscription", "is_cancel_enable"])

        card = self.card

        if card:
            card.is_active = False
            card.save(update_fields=["is_active"])


    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'


def get_file_name(instance, filename):
    return f'img/users/{instance.user.id}/{filename}'

class UserImage(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='images')
    image = models.ImageField(upload_to=get_file_name)
    thumbnail = models.ImageField(upload_to=get_file_name, null=True, blank=True) # editable=False,
    # description = models.CharField(max_lenth=255)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.image.url

    def delete(self):
        self.image.delete()
        self.thumbnail.delete()
        super().delete()

    def save(self, *args, **kwargs):

        if not self.thumbnail:

            if not self.make_thumbnail():
                # set to a default thumbnail
                # raise Exception('Could not create thumbnail - is the file type valid?')
                print('Error thumbnail')

        super().save(*args, **kwargs)

    def make_thumbnail(self):

        try:

            THUMB_SIZE = (650, 650)

            image = Image.open(self.image)
            image.thumbnail(THUMB_SIZE, Image.ANTIALIAS)

            thumb_name, thumb_extension = os.path.splitext(self.image.name)
            thumb_extension = thumb_extension.lower()

            thumb_filename = thumb_name + '_thumb' + thumb_extension

            if thumb_extension in ['.jpg', '.jpeg']:
                FTYPE = 'JPEG'
            elif thumb_extension == '.gif':
                FTYPE = 'GIF'
            elif thumb_extension == '.png':
                FTYPE = 'PNG'
            else:
                return False    # Unrecognized file type

            # Save thumbnail to in-memory file as StringIO
            temp_thumb = BytesIO()
            image.save(temp_thumb, FTYPE)
            temp_thumb.seek(0)

            # set save=False, otherwise it will run in an infinite loop
            self.thumbnail.save(thumb_filename, ContentFile(temp_thumb.read()), save=False)
            temp_thumb.close()

            return True

        except Exception as e:

            print(e)

    class Meta:
        verbose_name = 'Фото'
        verbose_name_plural = 'Фото'


class Config(SingletonModel):

    max_chats = models.IntegerField(default=10)
    max_msg_per_chat = models.IntegerField(default=3)

    about_service = RichTextUploadingField()

    def __str__(self):
        return "Настройки"

    class Meta:
        verbose_name = "Настройки"


class Report(models.Model):

    from_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')
    to_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')
    reason = models.CharField(max_length=255)
    text = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'Жалоба {self.id}'

    class Meta:
        verbose_name = 'Жалоба'
        verbose_name_plural = 'Жалобы'
