from django.shortcuts import render
from datetime import datetime, timedelta, date
from .models import  *
from rest_framework.response import Response

import os
import sys

import json

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import JSONParser, MultiPartParser, FormParser, FileUploadParser
from rest_framework.renderers import JSONRenderer
from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate
from rest_framework.permissions import IsAuthenticated
from django.core.mail import send_mail
from django.utils.crypto import get_random_string
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework import viewsets
from django.forms import modelformset_factory
from .forms import UserImageForm
from rest_framework.generics import CreateAPIView, DestroyAPIView, UpdateAPIView, ListAPIView, RetrieveAPIView
from django_filters import rest_framework as filters
from django.utils.timezone import now
from .permissions import isManagerPermission
from .filters import UserFilter
from .pagination import SearchUserPagination
from apps.permissions import IsManager
# from rest_framework.decorators import api_view
#
# from django.contrib.auth.tokens import PasswordResetTokenGenerator, default_token_generator
# from django.utils import six
#
# from django.utils.encoding import force_text
# from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
# from django.utils.encoding import force_bytes


import requests

from django.template.defaultfilters import date as _date
from django.core.paginator import Paginator
from django.db.models import Count, Min, Sum, Avg
from django.db.models.functions import TruncMonth, TruncDay, TruncWeek, TruncQuarter, \
    ExtractYear, ExtractQuarter, ExtractMonth, ExtractWeek, ExtractDay, Cast

from .serializers import *
# UserSerializer, LoginUserSerializer, UpdateUserSerializer, RecoveryUserSerializer, \
# ChoiceListSerializer, CitySerializer, CountrySerializer

import logging
logging.basicConfig(filename="./log.txt", level=logging.ERROR)
# Create your views here.


def index_view(request):

    host = request.META.get('HTTP_HOST')

    if host:
        if 'manager' in host:
            return render(request, 'manager.html')

    return render(request, 'index.html')


class RegisterUser(APIView):
    # permission_classes = (IsAuthenticated,)
    parser_classes = (JSONParser,)
    renderer_classes = (JSONRenderer,)
    serializer_class = UserRegistrationSerializer

    def post(self, request):

        serializer = self.serializer_class(data=request.data)

        if not serializer.is_valid(raise_exception=True):
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        user = serializer.save()

        refresh = RefreshToken.for_user(user)

        token = {
            'refresh': str(refresh),
            'access': str(refresh.access_token),
        }

        return Response(token)


class CheckRegUser(APIView):
    # permission_classes = (IsAuthenticated,)
    parser_classes = (JSONParser,)
    renderer_classes = (JSONRenderer,)
    serializer_class = CheckRegUserSerializer

    def post(self, request):

        serializer = self.serializer_class(data=request.data)

        if not serializer.is_valid(raise_exception=True):
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_200_OK)


class LoginUser(APIView):
    parser_classes = (JSONParser,)
    renderer_classes = (JSONRenderer,)
    serializer_class = LoginUserSerializer

    def post(self, request, *args, **kwargs):

        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid(raise_exception=True):
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        user = serializer.validated_data['user']

        if not user:
            return Response({"detail": "Ошибка авторизации"}, status=status.HTTP_401_UNAUTHORIZED)

        token, _ = Token.objects.get_or_create(user=user)

        return Response({'token': token.key})


class UpdateUser(APIView):
    parser_classes = (JSONParser,)
    renderer_classes = (JSONRenderer,)
    serializer_class = UpdateUserSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):

        serializer = self.serializer_class(data=request.data)

        if not serializer.is_valid(raise_exception=True):
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        user = serializer.update(request.user, serializer.validated_data)

        serializer = UserInfoSerializer(instance=user)

        return Response(serializer.data)


class UserInfo(APIView):
    parser_classes = (JSONParser,)
    renderer_classes = (JSONRenderer,)
    serializer_class = UserInfoSerializer
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):

        serializer = self.serializer_class(instance=request.user)

        return Response(serializer.data)


class RecoveryUser(APIView):
    parser_classes = (JSONParser,)
    renderer_classes = (JSONRenderer,)
    serializer_class = RecoveryUserSerializer

    def post(self, request, *args, **kwargs):

        serializer = self.serializer_class(data=request.data)
        if not serializer.is_valid(raise_exception=True):
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        user = serializer.validated_data['user']

        if user:
            new_password = User.objects.make_random_password()
            subject = "Восстановление пароля."
            message = f"Ваш новый пароль: {new_password}"
            res = user.email_user(subject, message)
            if res:
                user.set_password(new_password)
                user.save()

        return Response(status=status.HTTP_200_OK)


def choice_to_json(choice):
    data = dict()
    for item in choice:
        data[item[0]] = item[1]
    return data


class ChoiceList(APIView):
    parser_classes = (JSONParser,)
    renderer_classes = (JSONRenderer,)

    def get(self, request, *args, **kwargs):

        resp = dict()

        countries = Country.objects.all()
        serializer = CountrySerializer(countries, many=True)

        resp['countries'] = serializer.data
        resp['gender'] = choice_to_json(User.GENDER)
        resp['search_gender'] = choice_to_json(User.SEARCH_GENDER)
        resp['search_purpose'] = choice_to_json(User.SEARCH_PURPOSE)
        resp['education'] = choice_to_json(User.EDUCATION)
        resp['housing'] = choice_to_json(User.HOUSING)
        resp['appearance'] = choice_to_json(User.APPEARANCE)
        resp['children'] = choice_to_json(User.СHILDREN)
        resp['relationship'] = choice_to_json(User.RELATIONSHIP)
        resp['finance'] = choice_to_json(User.FINANCE)
        resp['alcohol'] = choice_to_json(User.ALCOHOL)
        resp['smoking'] = choice_to_json(User.SMOKING)

        return Response(resp)


class ChoiceList2(APIView):
    parser_classes = (JSONParser,)
    renderer_classes = (JSONRenderer,)

    def get(self, request, *args, **kwargs):

        resp = dict()

        countries = Country.objects.all()
        serializer = CountrySerializer(countries, many=True)

        resp['countries'] = serializer.data
        resp['fields'] = dict()
        resp['fields']['gender'] = choice_to_json(User.GENDER)
        resp['fields']['search_gender'] = choice_to_json(User.SEARCH_GENDER)
        resp['fields']['search_purpose'] = choice_to_json(User.SEARCH_PURPOSE)
        resp['fields']['education'] = choice_to_json(User.EDUCATION)
        resp['fields']['housing'] = choice_to_json(User.HOUSING)
        resp['fields']['appearance'] = choice_to_json(User.APPEARANCE)
        resp['fields']['children'] = choice_to_json(User.СHILDREN)
        resp['fields']['relationship'] = choice_to_json(User.RELATIONSHIP)
        resp['fields']['finance'] = choice_to_json(User.FINANCE)
        resp['fields']['alcohol'] = choice_to_json(User.ALCOHOL)
        resp['fields']['smoking'] = choice_to_json(User.SMOKING)

        return Response(resp)


class UserImages(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request, *args, **kwargs):
        serializer = UserImagesSerializer(instance=request.user)
        return Response(serializer.data)


class UploadUserImage(APIView):
    parser_classes = (MultiPartParser, )
    permission_classes = (IsAuthenticated,)
    renderer_classes = (JSONRenderer,)

    def post(self, request, *args, **kwargs):

        serializer = UserImageSerializer(data=request.data)

        if serializer.is_valid():
            user_image = serializer.save(user=request.user)
            if not request.user.avatar:
                # request.user.avatar = user_image.image
                request.user.avatar = user_image.thumbnail
                request.user.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DeleteUserImage(DestroyAPIView):
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset = UserImage.objects.filter(user=self.request.user)
        return queryset


class SearhUserList(ListAPIView):

    queryset = User.objects.filter(is_manager=False, is_staff=False)
    serializer_class = UserInfoSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = UserFilter
    permission_classes = (IsAuthenticated,)
    pagination_class = SearchUserPagination

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(city=self.request.user.city, is_block=False).exclude(id=self.request.user.id)


class PublicSearhUserList(ListAPIView):

    queryset = User.objects.filter(is_manager=False, is_staff=False)
    serializer_class = UserInfoSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = UserFilter
    # permission_classes = (IsAuthenticated,)
    pagination_class = SearchUserPagination

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(is_block=False)

class NewUsersStat(ListAPIView):
    serializer_class = NewUsersStatSerializer
    queryset = User.objects.filter(is_bot=False, is_manager=False, is_staff=False)
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = UserFilter
    filter_fields = {'created_at': ['lte', 'gte']}
    permission_classes = (IsAuthenticated, IsManager, )

    def list(self, request, *args, **kwargs):

        queryset = self.filter_queryset(self.get_queryset())

        queryset = queryset.\
            annotate(day=TruncDay('created_at')).values('day').\
            annotate(count=Count('id')).values('day', 'count').\
            order_by('day')

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class AnketaInfo(RetrieveAPIView):

    parser_classes = (JSONParser,)
    renderer_classes = (JSONRenderer,)
    serializer_class = UserInfoSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset = User.objects.filter(is_staff=False)
        # is_manager=False,
        return queryset

class ConfigInfo(RetrieveAPIView):

    parser_classes = (JSONParser,)
    renderer_classes = (JSONRenderer,)
    serializer_class = ConfigSerializer

    def get_object(self):
        return Config.get_solo()


class ReportViewSet(viewsets.ModelViewSet):

    parser_classes = (JSONParser,)
    renderer_classes = (JSONRenderer,)
    queryset = Report.objects.all()
    serializer_class = ReportSerializer
    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):

        if request.user.id != request.data['from_user']:
            return Response(status=status.HTTP_403_FORBIDDEN)

        return super().create(request, *args, **kwargs)


# TODO
# get next search user and dont repeat or random
# save search filter
# отмена подписки
# нужен ли метод установка аватарки? хранить аватарку в бд в отдельном поле или первый из UserImage,
# что делать с аватаркой при удалении из списка изображений
# config = SiteConfiguration.get_solo()
# from django.core.cache import cache
# Смотреть профили пользователей
#

