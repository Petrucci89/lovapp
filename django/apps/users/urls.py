from django.urls import path
from .views import *
    # , LoginView, UserInfoView, \
    # HoroscopeView, PayView, FCMDeviceView, UnitellerStatusView, \
    # PreauthPaymentUrlView, CancelSubscriptionView
from rest_framework.authtoken.views import obtain_auth_token

report_list = ReportViewSet.as_view({
    # 'get': 'list',
    'post': 'create',
})


urlpatterns = [

    path('registration/', RegisterUser.as_view()),
    path('check/', CheckRegUser.as_view()),
    # path('login/', LoginUser.as_view()),
    path('update/', UpdateUser.as_view()),
    path('info/', UserInfo.as_view()),
    path('recovery/', RecoveryUser.as_view()),
    path('choice_list/', ChoiceList.as_view()),
    path('choice_list2/', ChoiceList2.as_view()),
    path('images/', UserImages.as_view()),
    path('upload_image/', UploadUserImage.as_view()),
    path('delete_image/<int:pk>/', DeleteUserImage.as_view()),
    path('search/', SearhUserList.as_view()),
    path('public_search/', PublicSearhUserList.as_view()),
    path('anketa_info/<int:pk>/', AnketaInfo.as_view()),
    path('about_service/<int:pk>/', ConfigInfo.as_view()),
    path('reports/', report_list),

    path('new_users_stat/', NewUsersStat.as_view()),

]
