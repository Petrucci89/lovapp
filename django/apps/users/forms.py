from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField, AuthenticationForm
from django.utils.translation import gettext, gettext_lazy as _
from django.contrib.auth import get_user_model
from .models import UserImage

User = get_user_model()

class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('phone', 'email', 'name', 'birthday', 'city', 'height', 'weight',
                  'gender', 'search_gender', 'search_purpose', 'education',
                  'housing', 'appearance', 'children', 'relationship', 'finance',
                  'alcohol', 'smoking', 'is_premium', 'is_online', 'avatar',
                  'reccurent_payment_attempt', 'when_next_reccurent_payment_attempt',)

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField(
        label=_("Password"),
        help_text=_(
            "Raw passwords are not stored, so there is no way to see this "
            "user's password, but you can change the password using "
            "<a href=\"{}\">this form</a>."
        ),
    )

    class Meta:
        model = User
        fields = ('phone', 'email', 'name', 'birthday', 'city', 'height', 'weight',
                  'gender', 'search_gender', 'search_purpose', 'education',
                  'housing', 'appearance', 'children', 'relationship', 'finance',
                  'alcohol', 'smoking', 'is_premium', 'is_online', 'avatar',
                  'reccurent_payment_attempt', 'when_next_reccurent_payment_attempt',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        password = self.fields.get('password')
        if password:
            password.help_text = password.help_text.format('../password/')
        user_permissions = self.fields.get('user_permissions')
        if user_permissions:
            user_permissions.queryset = user_permissions.queryset.select_related('content_type')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial.get('password')


class UserImageForm(forms.ModelForm):

    class Meta:
        model = UserImage
        fields = ('image', )
