from rest_framework.pagination import CursorPagination


class SearchUserPagination(CursorPagination):
    cursor_query_param = 'id'
    page_size = 25
    ordering = 'id'
