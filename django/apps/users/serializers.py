from rest_framework import serializers
from .models import *
from django.contrib.auth import get_user_model, authenticate
import re
User = get_user_model()


from .models import User

class UserRegistrationSerializer(serializers.ModelSerializer):

    country = serializers.ReadOnlyField(source='city.country')
    avatar = serializers.SerializerMethodField('get_avatar_url')

    class Meta:
        model = User
        fields = (
            'id',
            'phone',
            'password',
            'email',
            'name',
            'is_premium',
            'is_online',
            'avatar',
            'is_cancel_subscription',
            'is_cancel_enable',
            'birthday',
            'country',
            'city',
            'height',
            'weight',
            'gender',
            'search_gender',
            'search_purpose',
            'education',
            'housing',
            'appearance',
            'children',
            'relationship',
            'finance',
            'alcohol',
            'smoking',
            'reg_platform',
            'utm',
            'created_at'
        )
        extra_kwargs = {
            'id': {'read_only': True},
            'phone': {'required': True},
            'password': {'write_only': True},
            'email': {'required': True},
            'name': {'required': True},
            'is_premium': {'read_only': True},
            'is_online': {'read_only': True},
            'avatar': {'read_only': True},
            'is_cancel_subscription': {'read_only': True},
            'is_cancel_enable': {'read_only': True},
            'birthday': {'required': True},
            'city': {'required': True},
            'height': {'required': True},
            'weight': {'required': True},
            'gender': {'required': True, 'allow_null': False, 'allow_blank': False},
            'search_gender': {'required': True, 'allow_null': False, 'allow_blank': False},
            'search_purpose': {'required': True, 'allow_null': False, 'allow_blank': False},
            'education': {'required': True, 'allow_null': False, 'allow_blank': False},
            'housing': {'required': True, 'allow_null': False, 'allow_blank': False},
            'appearance': {'required': True, 'allow_null': False, 'allow_blank': False},
            'children': {'required': True, 'allow_null': False, 'allow_blank': False},
            'relationship': {'required': True, 'allow_null': False, 'allow_blank': False},
            'finance': {'required': True, 'allow_null': False, 'allow_blank': False},
            'alcohol': {'required': True, 'allow_null': False, 'allow_blank': False},
            'smoking': {'required': True, 'allow_null': False, 'allow_blank': False},
            'created_at': {'read_only': True}
        }

    def get_avatar_url(self, obj):
        if obj.avatar:
            return obj.avatar.url
        else:
            return None

    def to_internal_value(self, data):

        if 'phone' in data:
            data['phone'] = data['phone'].replace(' ', '').replace('+', '').replace('(', '').replace(')', '').replace('-', '')
        elif 'email' in data:
            data['email'] = data['email'].lower()

        return super().to_internal_value(data)

    def validate_password(self, value):

        if len(value) < 8:
            raise serializers.ValidationError("Минимальная длинная пароля 8 символов.")

        return value

    def create(self, validated_data):

        user = get_user_model().objects.create_user(**validated_data)
        return user


class CheckRegUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'phone',
            'email',
            'password'
        )

        extra_kwargs = {
            'phone': {'required': True},
            'email': {'required': True},
            'password': {'required': True},
        }

    def to_internal_value(self, data):

        if 'phone' in data:
            data['phone'] = data['phone'].replace(' ', '').replace('+', '').replace('(', '').replace(')', '').replace('-', '')
        elif 'email' in data:
            data['email'] = data['email'].lower()

        return super().to_internal_value(data)

    def validate_password(self, value):

        if len(value) < 8:
            raise serializers.ValidationError("Минимальная длинная пароля 8 символов.")

        return value


class UserImageSerializer(serializers.ModelSerializer):

    url = serializers.URLField(source='image.url', read_only=True)
    thumbnail_url = serializers.SerializerMethodField('get_thumbnail_url')

    def get_thumbnail_url(self, obj):
        if obj.thumbnail:
            return obj.thumbnail.url
        else:
            return None

    # def to_representation(self, instance):
    #     return instance.image.url

    class Meta():
        model = UserImage
        fields = ('id', 'image', 'url', 'thumbnail_url', )

        extra_kwargs = {
            'image': {'write_only': True, 'required': True},
            'id': {'read_only': True},
        }


class UserInfoSerializer(serializers.ModelSerializer):

    country = serializers.ReadOnlyField(source='city.country.id')
    images = UserImageSerializer(many=True, read_only=True)
    avatar = serializers.SerializerMethodField('get_avatar_url')

    class Meta:
        model = User
        fields = (
            'id',
            'phone',
            'email',
            'name',
            'birthday',
            'age',
            'is_premium',
            'is_online',
            'avatar',
            'is_cancel_subscription',
            'is_cancel_enable',
            'images',
            'country',
            'city',
            'height',
            'weight',
            'gender',
            'search_gender',
            'search_purpose',
            'education',
            'housing',
            'appearance',
            'children',
            'relationship',
            'finance',
            'alcohol',
            'smoking',
            'created_at'
        )

    def get_avatar_url(self, obj):
        if obj.avatar:
            return obj.avatar.url
        else:
            return None


class CitySerializer(serializers.ModelSerializer):

    class Meta:
        model = City
        fields = (
            'id',
            'name'
        )


class CountrySerializer(serializers.ModelSerializer):

    cities = CitySerializer(many=True)

    class Meta:
        model = Country
        fields = (
            'id',
            'name',
            'cities'
        )


class UpdateUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'name',
            'birthday',
            'city',
            'height',
            'weight',
            'gender',
            'search_gender',
            'search_purpose',
            'education',
            'housing',
            'appearance',
            'children',
            'relationship',
            'finance',
            'alcohol',
            'smoking',
            'device_token',
        )


class RecoveryUserSerializer(serializers.Serializer):

    email = serializers.EmailField(required=True)

    def validate(self, attrs):

        try:
            email = attrs.get('email').lower()
            user = User.objects.get(email=email)
        except:
            user = None

        attrs['user'] = user
        return attrs


class LoginUserSerializer(serializers.Serializer):
    login = serializers.CharField(required=True)
    password = serializers.CharField(
        trim_whitespace=False,
        required=True
    )

    def validate(self, attrs):
        login = attrs.get('login')
        password = attrs.get('password')

        if login and password:
            user = authenticate(username=login, password=password)
            # if not user:
            #     msg = "Ошибка авторизации."
            #     raise serializers.ValidationError(msg, code='authorization')
        else:
            msg = "Поля login и password обязательны."
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs


class UserImagesSerializer(serializers.ModelSerializer):

    images = UserImageSerializer(many=True, read_only=True)

    class Meta():
        model = User
        fields = ('images',)


class NewUsersStatSerializer(serializers.Serializer):

    day = serializers.DateTimeField(format='%Y-%m-%d')
    count = serializers.IntegerField()


class ConfigSerializer(serializers.ModelSerializer):

    class Meta:
        model = Config
        fields = ('about_service',)


class ReportSerializer(serializers.ModelSerializer):

    # from_user_id = serializers.ReadOnlyField(source='from_user.id')

    class Meta:
        model = Report
        fields = (
            'id',
            'from_user',
            'to_user',
            'reason',
            'text',
            'created_at',
        )

        extra_kwargs = {
            'id': {'read_only': True},
            'created_at': {'read_only': True}
        }
