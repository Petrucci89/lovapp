from django_filters import rest_framework as filters
from django.contrib.auth import get_user_model, authenticate
from django.utils.timezone import now
from datetime import timedelta, datetime, date
from .models import User
User = get_user_model()


class UserFilter(filters.FilterSet):

    age = filters.RangeFilter(field_name="birthday", method='filter_age')
    is_fresh = filters.BooleanFilter(field_name="created_at", method='filter_fresh')
    country = filters.NumberFilter(field_name="city", method='filter_country')
    reg_platform = filters.MultipleChoiceFilter(choices=User.PLATFORM, field_name="reg_platform")

    def filter_age(self, queryset, name, value):

        current = now().date()
        min_date = date(current.year - value.start, current.month, current.day)
        max_date = date(current.year - value.stop, current.month, current.day)

        return queryset.filter(**{f'{name}__gte': max_date, f'{name}__lte': min_date,})

    def filter_fresh(self, queryset, name, value):

        from_date = now() - timedelta(days=User.FRESH_DELTA_DAYS)

        if value:
            return queryset.filter(**{f'{name}__gte': from_date})

        return queryset

    def filter_country(self, queryset, name, value):
        return queryset.filter(**{f'{name}__country__id': value})

    class Meta:
        model = User
        fields = ['search_purpose', 'gender', 'city', 'country', 'age', 'is_fresh', 'is_online', 'reg_platform', ]
