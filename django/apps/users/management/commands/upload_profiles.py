from django.core.management.base import BaseCommand
from datetime import datetime, timedelta
from django.utils import timezone
from django.contrib.auth import get_user_model
User = get_user_model()
from apps.users.models import City, UserImage
import random
import os
import sys
import shutil

class Command(BaseCommand):
    def handle(self, *args, **options):

        try:

            # обрабатываем рекурентный платеж

            base_path = '/core_project/anketas/women18_30'

            os.chdir(base_path)

            print(os.getcwd())

            for city in City.objects.filter(country__id=1, id__gt=19).order_by('id'):

                try:

                    if city.id in [1]:
                        anketa_count = 160
                        #continue
                    elif city.id in [2]:
                        anketa_count = 80
                        #continue
                    elif city.id in [3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19]:
                        anketa_count = 40
                        #continue
                    else:
                        anketa_count = 1
                        #print('quit')
                        #quit()

                    print(city, city.id)

                    for _ in range(anketa_count):

                        dirs = os.listdir('.')
                        if not dirs:
                            print('finish')
                            quit()

                        dir = dirs[0]

                        phone = '71110000000'
                        email1 = 'anketa_bot'
                        email2 = '@email.com'

                        f = open("/core_project/name-wooman.txt", "r", encoding="utf8")
                        men_names = f.read().splitlines()

                        height = 165 + random.randint(-8,18)
                        weight = height - 110 + random.randint(-5, 15)

                        password = User.objects.make_random_password(length=10)

                        user = User(
                            phone=phone,
                            # password=password,
                            email=email1 + email2,
                            name=random.choice(men_names),
                            birthday=timezone.now() - timedelta(days=random.randint(18 * 365, 30 * 365)),
                            city=city,
                            height=height,
                            weight=weight,
                            gender='F',
                            search_gender='M',
                            search_purpose=random.choice(User.SEARCH_PURPOSE)[0],
                            education=random.choice(User.EDUCATION)[0],
                            housing=random.choice(User.HOUSING)[0],
                            appearance=random.choice(User.APPEARANCE)[0],
                            children=random.choice(User.СHILDREN)[0],
                            relationship=random.choice(User.RELATIONSHIP)[0],
                            finance=random.choice(User.FINANCE)[0],
                            alcohol=random.choice(User.ALCOHOL)[0],
                            smoking=random.choice(User.SMOKING)[0],
                            is_bot=True
                        )

                        user.save()

                        user.phone = str(int(phone) + user.id)
                        user.set_password(password)
                        user.email = email1 + str(user.id) + email2
                        user.save()

                        print(user, 'user created')

                        i = 1
                        for image_name in os.listdir(f'./{dir}'):

                            try:

                                # print(image_name)

                                image = UserImage(user=user,
                                                  image=f'img/users/{user.id}/{image_name}')

                                image.save()

                                if i == 1:

                                    user.avatar = f'img/users/{user.id}/{image_name}'
                                    user.save()

                                i += 1

                                if not os.path.exists(f"/core_project/media/img/users/{user.id}"):
                                    os.makedirs(f"/core_project/media/img/users/{user.id}")

                                os.replace(base_path + f'/{dir}/{image_name}',
                                           f"/core_project/media/img/users/{user.id}/{image_name}")

                            except Exception as e:

                                exc_type, exc_obj, exc_tb = sys.exc_info()
                                # print("sys.exc_info() : ", sys.exc_info())
                                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                                print(exc_type, fname, exc_tb.tb_lineno)
                                print(str(e))

                        shutil.rmtree(base_path + f'/{dir}/')

                        # quit()

                except Exception as e:

                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    # print("sys.exc_info() : ", sys.exc_info())
                    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                    print(exc_type, fname, exc_tb.tb_lineno)
                    print(str(e))

                # quit()

        except Exception as e:

            exc_type, exc_obj, exc_tb = sys.exc_info()
            # print("sys.exc_info() : ", sys.exc_info())
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            print(str(e))


# import os
#
# os.chdir()
# os.getcwd()
# os.listdir('.')
#
#
# os.rename("path/to/current/file.foo", "path/to/new/destination/for/file.foo")
# shutil.move("path/to/current/file.foo", "path/to/new/destination/for/file.foo")
# os.replace("path/to/current/file.foo", "path/to/new/destination/for/file.foo")
#
# import shutil
# shutil.rmtree('/path/to/your/dir/')
#
# import os
# os.system("rm -r /home/user/folder_name")
