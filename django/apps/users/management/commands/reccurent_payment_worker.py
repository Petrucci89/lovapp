from django.core.management.base import BaseCommand
from datetime import datetime
from django.contrib.auth import get_user_model
User = get_user_model()

class Command(BaseCommand):
    def handle(self, *args, **options):

        # обрабатываем рекурентный платеж

        for user in User.objects.filter(reccurent_payment_attempt__lt=8,
                                        when_next_reccurent_payment_attempt__lt=datetime.today()):

            user.make_reccurent_payment()

