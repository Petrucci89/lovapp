from django.core.management.base import BaseCommand
from datetime import datetime, timedelta
from django.utils import timezone
from django.contrib.auth import get_user_model
User = get_user_model()
from apps.users.models import User
import random
import os
import sys
import shutil

class Command(BaseCommand):
    def handle(self, *args, **options):

        try:

            bots_count = int(User.objects.filter(is_bot=True).count() * 0.3)

            User.objects.filter(is_bot=True).update(is_online=False)

            random_bots_id_list = User.objects.filter(is_bot=True).values_list('id', flat=True).order_by('?')[:bots_count]

            User.objects.filter(is_bot=True, id__in=random_bots_id_list).update(is_online=True)

        except Exception as e:

            exc_type, exc_obj, exc_tb = sys.exc_info()
            # print("sys.exc_info() : ", sys.exc_info())
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            print(str(e))
