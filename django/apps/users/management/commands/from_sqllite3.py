from django.core.management.base import BaseCommand
from datetime import datetime, timedelta
from django.utils import timezone
from django.contrib.auth import get_user_model
User = get_user_model()
from apps.users.models import *
from apps.chat.models import *
from apps.payments.models import *
import random
import os
import sys
import shutil
import sqlite3

class Command(BaseCommand):
    def handle(self, *args, **options):

        try:

            location = 'db.sqlite3'
            table_name = 'white_list'

            conn = sqlite3.connect(location)
            c = conn.cursor()

            sql = 'SELECT * FROM payments_recurrentpayment'
            rows = c.execute(sql)
            # print(rows)
            for row in rows:
                print(row)

                a = RecurrentPayment(order_id=row[0],amount=row[1],is_paid=row[2],
                                     request_log=row[3],created_at=row[4],card_id=row[5])
                a.save()

        except Exception as e:

            exc_type, exc_obj, exc_tb = sys.exc_info()
            # print("sys.exc_info() : ", sys.exc_info())
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            print(str(e))


# def add_quote(quote):
#     global WHITE_LIST
#
#     sql = 'insert into ' + table_name + ' (quote) values ("%s")' % (quote)
#     c.execute(sql)
#     conn.commit()
#
# def init_white_list():
#     global WHITE_LIST
#     sql = 'select * from ' + table_name
#     rows = c.execute(sql)
#     # print(rows)
#     for row in rows:
#         WHITE_LIST.add(row[0])
#     # print(WHITE_LIST)
