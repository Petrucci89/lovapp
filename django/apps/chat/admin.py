from django.contrib import admin
from .models import *
# Register your models here.

@admin.register(Dialog)
class DialogAdmin(admin.ModelAdmin):
    list_display = ('user', 'peer_user', 'unread_count', 'last_message', 'last_update_at', 'last_seen_at', 'created_at', )
    ordering = ('-last_update_at', )

@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('from_user', 'to_user', 'text', 'image', 'created_at', )
