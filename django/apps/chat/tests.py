from django.test import TestCase
import asyncio
# Create your tests here.

import socketio


# asyncio
sio = socketio.AsyncClient()



@sio.on('connect', namespace='/users')
async def on_connect():
    print()
    print("I'm connected to the /users namespace!")


async def run():
    await sio.connect('https://lovapp.ru/socket.io/')

async def send_mes():
    await asyncio.sleep(3)
    print(123)
    await sio.emit('message', {'foo': 'bar'}, namespace='/users')
    print(234)


loop = asyncio.get_event_loop()
loop.run_until_complete(run())
loop.run_until_complete(send_mes())
loop.run_forever()
