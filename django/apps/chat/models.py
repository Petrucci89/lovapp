from django.db import models
from django.contrib.auth import get_user_model
User = get_user_model()
from django.db.models import Q
from django.db import transaction

# Create your models here.

def get_file_name(instance, filename):
    print(instance.from_user.id, instance.to_user.id)
    l = [instance.from_user.id, instance.to_user.id]
    l.sort()
    return f'img/msg/{l[0]}_{l[1]}/{instance.from_user.id}_{instance.to_user.id}_{filename}'


class Dialog(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='dialogs')
    peer_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')
    unread_count = models.PositiveIntegerField(default=0)
    last_message = models.ForeignKey("Message", blank=True, null=True, on_delete=models.SET_NULL)
    last_update_at = models.DateTimeField(auto_now=True, db_index=True)
    last_seen_at = models.DateTimeField(auto_now_add=True)
    created_at = models.DateTimeField(auto_now_add=True)

    @property
    def unread(self):
        return self.unread_count > 0

    @property
    def peer_last_seen_at(self):
        try:
            peer_d = Dialog.objects.get(user=self.peer_user, peer_user=self.user)
        except:
            return None
        return peer_d.last_seen_at

    @classmethod
    def create_both_dialogs(cls, from_user, to_user, m):

        with transaction.atomic():

            d1 = Dialog.objects.create(user=from_user,
                                       peer_user=to_user,
                                       last_message=m)

            d2 = Dialog.objects.create(user=to_user,
                                       peer_user=from_user,
                                       last_message=m,
                                       unread_count=1)

            return d1, d2

    # @property
    # def last_mes(self):
    #     return Message.objects.filter(chat=self).order_by('-created_at').first()

    def __str__(self):
        return f'Диалог {self.id}'

    class Meta:
        verbose_name = 'Чат'
        verbose_name_plural = 'Чаты'
        unique_together = ('user', 'peer_user',)


class Message(models.Model):

    from_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')
    to_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='+')
    # to_id если юзер или чат или канал в зависимости от айди отриц или полож понимать юзер или чат
    text = models.TextField(blank=True, null=True)
    image = models.ImageField(upload_to=get_file_name, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, db_index=True)

    def __str__(self):
        return f'Сообщение {self.id}'

    # @classmethod
    # def is_first_message(cls, from_user_id, to_user_id):
    #     exists = cls.objects.\
    #         filter(Q(from_user__id=from_user_id, to_user__id=to_user_id) |
    #                Q(from_user__id=to_user_id, to_user__id=from_user_id)).\
    #         exists()
    #
    #     if not exists:
    #         return True
    #     return False

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
