from rest_framework.pagination import CursorPagination


class UserDialogPagination(CursorPagination):
    cursor_query_param = 'last_update_at'
    page_size = 50
    ordering = '-last_update_at'


class MessagePagination(CursorPagination):
    cursor_query_param = 'created_at'
    page_size = 50
    ordering = '-created_at'
