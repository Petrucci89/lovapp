from rest_framework import serializers
from .models import *
from django.contrib.auth import get_user_model, authenticate
import re
User = get_user_model()


class DialogUserInfoSerializer(serializers.ModelSerializer):

    avatar = serializers.SerializerMethodField('get_avatar_url')

    class Meta:
        model = User
        fields = (
            'id',
            'name',
            'avatar',
            'is_online',
        )

    def get_avatar_url(self, obj):
        if obj.avatar:
            return obj.avatar.url
        else:
            return None

class MessageSerializer(serializers.ModelSerializer):

    from_user_id = serializers.ReadOnlyField(source='from_user.id')
    to_user_id = serializers.ReadOnlyField(source='to_user.id')
    created_at = serializers.DateTimeField(format='%d.%m.%Y %H:%M:%S.%f')
    image = serializers.SerializerMethodField('get_image_url')

    class Meta:
        model = Message
        fields = (
            'id',
            'from_user_id',
            'to_user_id',
            'text',
            'image',
            'created_at'
        )

    def get_image_url(self, obj):
        if obj.image:
            return obj.image.url
        else:
            return None


class DialogSerializer(serializers.ModelSerializer):

    peer_user = DialogUserInfoSerializer()
    last_message = MessageSerializer()
    last_update_at = serializers.DateTimeField(format='%d.%m.%Y %H:%M:%S.%f')
    # peer_last_updated_at = serializers.DateTimeField(format='%d.%m.%Y %H:%M:%S.%f', source='peer_last_seen_at')
    peer_last_seen_at = serializers.DateTimeField(format='%d.%m.%Y %H:%M:%S.%f')

    class Meta:
        model = Dialog
        fields = (
            'id',
            'peer_user',
            'unread',
            'last_message',
            'last_update_at',
            'peer_last_seen_at'
        )


class ManagerDialogSerializer(serializers.ModelSerializer):

    bot_user = DialogUserInfoSerializer(source='user')
    peer_user = DialogUserInfoSerializer()
    last_message = MessageSerializer()
    last_update_at = serializers.DateTimeField(format='%d.%m.%Y %H:%M:%S.%f')
    peer_last_seen_at = serializers.DateTimeField(format='%d.%m.%Y %H:%M:%S.%f')

    class Meta:
        model = Dialog
        fields = (
            'id',
            'bot_user',
            'peer_user',
            'unread',
            'last_message',
            'last_update_at',
            'peer_last_seen_at'
        )
