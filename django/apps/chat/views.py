from django.shortcuts import render
from datetime import datetime, timedelta, date
from .models import  *
from rest_framework.response import Response

import os
import sys

import json

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import JSONParser, MultiPartParser, FormParser, FileUploadParser
from rest_framework.renderers import JSONRenderer
from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate
from rest_framework.permissions import IsAuthenticated
from apps.permissions import IsManager
from django.core.mail import send_mail
from django.utils.crypto import get_random_string
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.generics import CreateAPIView, DestroyAPIView, UpdateAPIView, ListAPIView
from django_filters import rest_framework as filters
from django.utils.timezone import now
from django.db.models import Q
from .serializers import *
from .pagination import *

# Create your views here.

#from rest_framework_simplejwt.authentication import JWTAuthentication


class UserDialogList(ListAPIView):

    serializer_class = DialogSerializer
    permission_classes = (IsAuthenticated,)
    pagination_class = UserDialogPagination

    def get_queryset(self):
        return Dialog.objects.\
            filter(user=self.request.user).\
            order_by('-last_update_at')


class MessageList(ListAPIView):

    serializer_class = MessageSerializer
    permission_classes = (IsAuthenticated,)
    pagination_class = MessagePagination

    def get_queryset(self):

        #user_id = self.kwargs.get('user_id')
        user_id = self.request.user.id
        peer_user_id = self.kwargs.get('peer_user_id')

        # if self.request.user.id != user_id:
        #     return Response(status=status.HTTP_400_BAD_REQUEST)

        return Message.objects.\
            filter(Q(from_user__id=user_id, to_user__id=peer_user_id) |
                   Q(from_user__id=peer_user_id, to_user__id=user_id)).\
            order_by('-created_at')


class ManagerMessageList(ListAPIView):

    serializer_class = MessageSerializer
    permission_classes = (IsAuthenticated, IsManager)
    pagination_class = MessagePagination

    def get_queryset(self):

        # user_id = self.kwargs.get('user_id')
        # user_id = self.request.user.id
        bot_user_id = self.kwargs.get('bot_user_id')
        peer_user_id = self.kwargs.get('peer_user_id')

        # if self.request.user.id != user_id:
        #     return Response(status=status.HTTP_400_BAD_REQUEST)

        return Message.objects.\
            filter(Q(from_user__id=bot_user_id, to_user__id=peer_user_id) |
                   Q(from_user__id=peer_user_id, to_user__id=bot_user_id)).\
            order_by('-created_at')


class ManagerDialogList(ListAPIView):

    serializer_class = ManagerDialogSerializer
    permission_classes = (IsAuthenticated, IsManager)
    # pagination_class = UserDialogPagination

    def get_queryset(self):
        return Dialog.objects.\
            filter(user__is_bot=True).\
            order_by('-last_update_at')



# TODO
# прочитано, уведомление о новом сообщении
# create chat, get message, cursor pag or page pag
# ограничения 10 чатов 3 сообщения
# размер картинки
