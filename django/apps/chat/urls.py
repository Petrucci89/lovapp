from django.urls import path

from .views import *

urlpatterns = [
    path('user_dialogs/', UserDialogList.as_view(), name='user_dialogs'),
    path('messages/<int:peer_user_id>/', MessageList.as_view(), name='messages'),

    path('manager_dialogs/', ManagerDialogList.as_view(), name='manager_dialogs'),
    path('manager_messages/<int:bot_user_id>/<int:peer_user_id>/', ManagerMessageList.as_view(), name='manager_messages'),
]
