var gulp = require('gulp'),
	rsync = require('gulp-rsync');


gulp.task('deploy-index', function() {
	return gulp.src('build/*.html')
		.pipe(rsync({
			root: 'build/',
			hostname: 'lovappuser@lovapp.ru',
			destination: '/home/lovappuser/django/templates',
			archive: true,
			slient: false,
			compress: true
		}));
});

gulp.task('deploy-static' , function() {
	return gulp.src('build/static/**')
		.pipe(rsync({
			root: 'build/static',
			hostname: 'lovappuser@lovapp.ru',
			destination: '/home/lovappuser/django/static/site/static',
			archive: true,
			slient: false,
			compress: true,
		}));
});

// gulp.task('deploy-index-test', function() {
// 	return gulp.src('build/*.html')
// 		.pipe(rsync({
// 			root: 'build/',
// 			hostname: 'root@95.179.241.146',
// 			destination: '/home/botuser/django/templates',
// 			archive: true,
// 			slient: false,
// 			compress: true
// 		}));
// });
//
// gulp.task('deploy-static-test' , function() {
// 	return gulp.src('build/static/**')
// 		.pipe(rsync({
// 			root: 'build/static',
// 			hostname: 'root@95.179.241.146',
// 			destination: '/home/botuser/django/static',
// 			archive: true,
// 			slient: false,
// 			compress: true,
// 		}));
// });


gulp.task('deploy', gulp.series('deploy-index', 'deploy-static'));
// gulp.task('deploy-test', gulp.series('deploy-index-test', 'deploy-static-test'));
