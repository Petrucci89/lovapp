export default (countries, country, city) => {
    if (!countries || !country || !city) return null;
    const countryIndex = countries.findIndex(element => element.id === country);
    const cityIndex = countries[countryIndex].cities.findIndex(element => element.id === city);
    return `${countries[countryIndex].name}, ${countries[countryIndex].cities[cityIndex].name}`;
}
