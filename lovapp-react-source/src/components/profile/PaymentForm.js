import React, { useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import { connect } from 'react-redux';
import {
    Grid,
    Button,
    Typography,
    CircularProgress,
    useMediaQuery,
} from '@material-ui/core';
import MuiTextField from '@material-ui/core/TextField';
import { useTheme } from '@material-ui/core/styles';
import { Alert } from '@material-ui/lab';
import { fieldToTextField, TextFieldProps } from 'formik-material-ui';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';

import api from '../../api';
import axios from 'axios';
import { togglePaymentFormLoading } from '../../actions/profile';

const paymentSchema = Yup.object().shape({
    number: Yup.string().required('Это поле обязательно!'),
    month: Yup.number('Введите число').required('!'),
    year: Yup.number('Введите число').required('!'),
    cvc: Yup.number('Введите число').required('!'),
});

const PaymentForm = ({ inAppPayment, showPaymentFormLoading, togglePaymentFormLoading }) => {
    const classes = useStyles();
    const theme = useTheme();
    const upMd = useMediaQuery(theme.breakpoints.up('md'));
    let [error, setError] = useState(null);


    const tokenize = async (number, month, year, cvc) => {
        try {
            const checkout = window.YooMoneyCheckout('791143');
            const token = await checkout.tokenize({
                number: number.trim(),
                cvc: cvc.trim(),
                month: month.trim(),
                year: year.trim(),
            }).then(res => {
                if (res.status === 'success') {
                    const { paymentToken } = res.data.response;
                    return paymentToken;
                } else {
                    console.log(res.data)
                }
            });
            return token;
        } catch (error) {
            console.log(error);
            return null;
        }
    }

    const handlePayment = async (values) => {
        if (showPaymentFormLoading) return;
        togglePaymentFormLoading(true);
        setError(null);
        const token = await tokenize(values.number, values.month, values.year, values.cvc);
        if (!token) {
            setError('Что-то пошло не так... Повторите попытку или попробуйте другую карту.');
            togglePaymentFormLoading(false);
            return;
        }
        if (inAppPayment) {
            handleMobilePayment(values, token);
        } else {
            handleWebPayment(values, token);
        }
    }

    const handleWebPayment = async ({ number, month, year, cvc }, token) => {
        try {
            const apiResponse = await api.post('/api/payments/get_payment_link/', { token });
            const { status, paid, confirmation, /* cancellation_details */ } = apiResponse.data;
            if (confirmation && confirmation.confirmation_url) {
                window.location.href = confirmation.confirmation_url
            } else if (paid) {
                window.location.href = '/payment-success'
            } else if (status === 'canceled') {
                setError('Что-то пошло не так... Повторите попытку или попробуйте другую карту.');
            }
            togglePaymentFormLoading(false);
        } catch (error) {
            console.log(error);
        }
    }

    const handleMobilePayment = async ({ number, month, year, cvc }, token) => {
        const user_id = (new URLSearchParams(window.location.search)).get('user_id');
        if (!user_id) {
            setError('Произошла ошибка. Попробуйте позже. (no user_id)');
            togglePaymentFormLoading(false);
            return;
        }
        try {
            const apiResponse = await axios.post('/api/payments/get_app_payment_link/', { user_id, token });
            const { status, paid, confirmation, /* cancellation_details */ } = apiResponse.data;
            if (confirmation && confirmation.confirmation_url) {
                window.location.href = confirmation.confirmation_url
            } else if (paid) {
                window.location.href = '/payment-success'
            } else if (status === 'canceled') {
                setError('Что-то пошло не так... Повторите попытку или попробуйте другую карту.');
            }
            togglePaymentFormLoading(false);
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <div>
            {error && <Alert severity="error" style={{ marginBottom: 16 }}>{error}</Alert>}
            <Formik
                initialValues={{ number: '', month: '', year: '', cvc: '' }}
                validationSchema={paymentSchema}
                onSubmit={handlePayment}
                onChange={(event) => console.log(event)}
                style={{ maxWidth: upMd ? 400 : 'none' }}
            >
                {({ isSubmitting, values, errors }) => (
                    <Form>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <Field
                                    name="number"
                                    id="credit-card-number"
                                    component={CreditCardTextField}
                                    variant="outlined"
                                    size="small"
                                    margin="dense"
                                    fullWidth
                                    label="Номер карты"
                                    autoComplete="cc-number"
                                    style={{ marginBottom: 4, marginTop: 4 }}
                                />
                            </Grid>
                            <Grid item xs={6} style={{ textAlign: 'left' }}>
                                <Field
                                    name="month"
                                    id="credit-card-month"
                                    component={CreditCardTextField}
                                    className={classes.TextField}
                                    variant="outlined"
                                    size="small"
                                    margin="dense"
                                    label="ММ"
                                    autoComplete="cc-exp-month"
                                    style={{ marginBottom: 4, marginTop: 4, display: 'inline-block', width: 60 }}
                                />
                                <div style={{ display: 'inline-flex', marginTop: 11, fontSize: 18, color: 'rgba(0, 0, 0, 0.50)'}}>&nbsp;/&nbsp;</div>
                                <Field
                                    name="year"
                                    id="credit-card-year"
                                    component={CreditCardTextField}
                                    className={classes.TextField}
                                    variant="outlined"
                                    size="small"
                                    margin="dense"
                                    label="ГГ"
                                    autoComplete="cc-exp-year"
                                    style={{ marginBottom: 4, marginTop: 4, display: 'inline-block', width: 60 }}
                                />
                            </Grid>
                            <Grid item xs={6} style={{ textAlign: 'right' }}>
                                <Field
                                    name="cvc"
                                    id="credit-card-cvc"
                                    component={CreditCardTextField}
                                    className={classes.TextField}
                                    variant="outlined"
                                    size="small"
                                    margin="dense"
                                    label="CVC"
                                    autoComplete="cc-csc"
                                    style={{ marginBottom: 4, marginTop: 4, marginLeft: 'auto', display: 'inline-block', width: 60 }}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Button
                                    color="secondary"
                                    id="payment-form-submit"
                                    fullWidth
                                    size="large"
                                    variant="contained"
                                    type="sumbmit"
                                    className={classes.SubmitButtom}
                                >
                                    {showPaymentFormLoading
                                        ? <CircularProgress color="inherit" style={{ color: '#fff' }} size={32} />
                                        : "Оплатить 1р"
                                    }
                                </Button>
                                <Typography
                                    variant="caption"
                                    align="center"
                                    component="p"
                                    gutterBottom
                                    style={{ marginBottom: 24 }}
                                >
                                    Стоимость доступа 750 руб. в месяц
                                </Typography>
                            </Grid>
                            {/*
                            <Grid item xs={12}>
                                <Typography
                                    variant="caption"
                                    align="center"
                                    component="p"
                                    gutterBottom
                                    style={{ marginBottom: 24 }}
                                >
                                    Продолжая, вы принимаете Правила использования<br />
                                    и подтверждаете, что вам 18 или более
                                </Typography>
                            </Grid>
                            */}
                        </Grid>
                    </Form>
                )}
            </Formik>
        </div>
    );
};

const formatCardNumber = value => {
    const regex = /^(\d{0,4})(\d{0,4})(\d{0,4})(\d{0,4})$/g;
    let onlyNumbers = value
        .replace(/[^\d]/g, '')
        .replace(regex, (regex, $1, $2, $3, $4) =>
            [$1, $2, $3, $4].filter(group => !!group).join(' ')
        );

    if (onlyNumbers.length >= 19) {
        onlyNumbers = onlyNumbers.slice(0, 19);
        document.querySelector('#credit-card-month').focus();
    }
    return onlyNumbers;
}

const formatCardDateAndCVC = (value, name) => {
    let onlyNumbers = value.replace(/[^\d]/g, '');

    if ((name === 'month' || name === 'year') && onlyNumbers.length >= 2) {
        onlyNumbers = onlyNumbers.slice(0, 2);
        if (name === 'month') {
            document.querySelector('#credit-card-year').focus();
        } else if (name === 'year') {
            document.querySelector('#credit-card-cvc').focus();
        }
    } else if (name === 'cvc' && onlyNumbers.length >= 3) {
        onlyNumbers = onlyNumbers.slice(0, 3);
        document.querySelector('#payment-form-submit').focus();
    }
    return onlyNumbers;
}

const CreditCardTextField = (props: TextFieldProps) => {
    const {
        form: { setFieldValue },
        field: { name },
    } = props;

    const onChange = React.useCallback(
        event => {
            const value = name === 'number'
                ? formatCardNumber(event.target.value)
                : formatCardDateAndCVC(event.target.value, name);
            setFieldValue(name, value || '');
        },
        [setFieldValue, name]
    );
    return <MuiTextField {...fieldToTextField(props)} onChange={onChange} />;
}

const useStyles = makeStyles((theme) => ({
    TextField: {
        '& .MuiFormHelperText-root': {
            display: 'none'
        }
    },
    SubmitButtom: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        minWidth: 300,
        marginBottom: 16,
        marginTop: 16,
        marginRight: 'auto',
        marginLeft: 'auto'
    }
}));

const mapStateToProps = ({ profile }) => ({
    showPaymentFormLoading: profile.showPaymentFormLoading
});

export default connect(mapStateToProps, { togglePaymentFormLoading })(PaymentForm);
