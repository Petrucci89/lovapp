import React, { useState } from 'react';
import { Paper, Grid, Typography, Divider } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { connect } from 'react-redux';

import './Search.sass';
import generateLocation from '../../helpers/generateLocation';
import Trait from '../profile/Trait';

const SearchProfile = ({ questions, user }) => {
}

const mapStateToProps = ({ profile }) => ({
    questions: profile.questions
});

export default connect(mapStateToProps)(SearchProfile);
