import React from 'react';
import { Fab, Typography, Grid } from '@material-ui/core';

import icon1 from '../../../assets/final-stage-1.png';
import icon2 from '../../../assets/final-stage-3.png';
import icon3 from '../../../assets/final-stage-2.png';

export default ({ handleSubmit }) => {
    return (
        <div className="text-center">
            <Typography
                variant="h5"
                align="center"
                style={{ marginBottom: 48 }}
            >
                Завершение регистрации
            </Typography>
            <Grid container spacing="10">
                <Grid item xs="12" md="4">
                    <Feature
                        title="Просматривайте анкеты пользователей,"
                        descr="которые находятся поблизости"
                        icon={icon1}
                    />
                </Grid>
                <Grid item xs="12" md="4">
                    <Feature
                        title="Начинай общение"
                        descr="с понравившимися пользователями"
                        icon={icon2}
                    />
                </Grid>
                <Grid item xs="12" md="4">
                    <Feature
                        title="Договаривайтесь о встрече,"
                        descr="если ваши симпатии совпадают"
                        icon={icon3}
                    />
                </Grid>
            </Grid>
            <Fab
                size="large"
                variant="extended"
                color="secondary"
                className="SignupForm-finish-btn"
                onClick={handleSubmit}
                style={{ width: 320, marginTop: 48 }}
            >
                Завершить
            </Fab>
        </div>
    );
};

const Feature = ({ title, descr, icon }) => (
    <div className="SignupForm-feature">
        <img src={icon} style={{ marginBottom: 16 }} alt="" />
        <Typography variant="subtitle1" style={{ fontWeight: 'bold' }}>{title}</Typography>
        <Typography variant="body1">{descr}</Typography>
    </div>
);
