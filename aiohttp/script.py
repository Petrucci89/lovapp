import asyncio
import aioredis
from aiohttp import web
from asgiref.sync import sync_to_async
import socketio
import os, sys
redis = None
import pytz
import logging
import django
import os, sys
from django.utils import timezone
logging.basicConfig(filename="./log.txt", level=logging.ERROR)
from datetime import datetime
TIME_ZONE = 'Europe/Moscow'
LOCAL_TZ = pytz.timezone(TIME_ZONE)
import aiohttp


def localize_dt(dt: datetime) -> datetime:
    return dt.replace(tzinfo=pytz.utc).astimezone(LOCAL_TZ)


async def init_redis():
    global redis
    redis = await aioredis.create_redis_pool('redis://redis:6379')
    # await create_tables()


FCM_END_POINT = "https://fcm.googleapis.com/fcm/send"
FCM_API_KEY = 'AAAAHR7VHVo:APA91bEdiuSUzX17y6dNBhBztxKTqxx60inmd98aHLZOR1GMNF9OvyBrWzWmAkFfHWv_0TEqgkNDryJvWI-3fjbP0Y-S6KGlZhaOkP7l1BOCYegWJerB5ZfY29Mf-rF5BwTm8t4BpPYj'

HEADERS = {
    "Content-Type": "application/json",
    "Authorization": "key=" + FCM_API_KEY,
}

async def send_notification(device_token, from_user, to_user, message):

    data = {
        'to': device_token,
        'priority': 'high',
        'data': {
            'message_title':'Новое сообщение',
            'message_body': f'Пользователь {from_user.name} прислал(а) вам сообщение',
            'from_user_id': from_user.id,
            'to_user_id': to_user.id,
            'message_id': message.id
        },
        'notification': {
            'title': 'Новое сообщение',
            'body': f'Пользователь {from_user.name} прислал(а) вам сообщение'
        },
    }

    try:

        async with aiohttp.ClientSession() as session:
            async with session.post(FCM_END_POINT, headers=HEADERS, json=data, timeout=5) as resp:
                if resp.status != 200:
                    resp_text = await resp.text()
                    logging.error(f"{resp.status} resp_text: {resp_text}")

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        # print("sys.exc_info() : ", sys.exc_info())
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        print(str(e))
        logging.error("send_notification {} {} {} \n {}".format(exc_type, fname, exc_tb.tb_lineno, str(e)))



# mgr = socketio.AsyncRedisManager('redis://redis:6379')
# sio = socketio.AsyncServer(client_manager=mgr)
# docker exec -it e0c061a5700bfa400f8f24b redis-cli

mgr = socketio.AsyncRedisManager('redis://redis:6379')
sio = socketio.AsyncServer(async_mode='aiohttp',
                           client_manager=mgr,
                           cors_allowed_origins=[])
# 'lovapp.ru', 'manager.lovapp.ru'
# sio = socketio.AsyncServer(async_mode='aiohttp', client_manager=mgr)
app = web.Application()
sio.attach(app)


async def go():
    redis = await aioredis.create_redis_pool('redis://redis:6379')
    await redis.set('my-key', 'value')
    val = await redis.get('my-key', encoding='utf-8')
    print(val)
    redis.close()
    await redis.wait_closed()


# TODO
# rename aiohttp
# add update is_online = False before start app
# test speed ayncpg and django orm
# NGINX убрать CORS
# get_solo config
# => asyncpg db

#settings.configure(default_settings=myapp_defaults)
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core_project.settings')
os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"

# Now this script or any imported module can use any part of Django it needs.

sys.path.append("/aiohttp/django_proj/")

django.setup()

from rest_framework_simplejwt.authentication import JWTAuthentication

from rest_framework_simplejwt.tokens import AccessToken


jwt_auth = JWTAuthentication()


from apps.chat.models import Message, Dialog
from apps.chat.serializers import DialogSerializer
from apps.users.models import User, Config


def decode_base64_file(from_user_id, to_user_id, data):

    try:

        def get_file_extension(file_name, decoded_file):
            import imghdr

            extension = imghdr.what(file_name, decoded_file)
            extension = "jpg" if extension == "jpeg" else extension

            return extension

        from django.core.files.base import ContentFile
        import base64
        import six
        import uuid

        # Check if this is a base64 string
        if isinstance(data, six.string_types):
            # Check if the base64 string is in the "data:" format
            if 'data:' in data and ';base64,' in data:
                # Break out the header from the base64 content
                header, data = data.split(';base64,')

            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(data)
            except TypeError:
                TypeError('invalid_image')

            # Generate file name:
            l = [from_user_id, to_user_id]
            l.sort()

            file_name = f"img/msg/{l[0]}_{l[1]}/{from_user_id}_{to_user_id}_{uuid.uuid4().hex}"
            # Get the file name extension:
            file_extension = get_file_extension(file_name, decoded_file)

            complete_file_name = "%s.%s" % (file_name, file_extension, )

            os.makedirs(os.path.dirname(f"/aiohttp/django_proj/media/img/msg/{l[0]}_{l[1]}/"), exist_ok=True)
            with open("/aiohttp/django_proj/media/" + complete_file_name, 'wb') as f:
                f.write(decoded_file)

            return complete_file_name
            #return ContentFile(decoded_file, name=complete_file_name)

    except Exception as e:

            exc_type, exc_obj, exc_tb = sys.exc_info()
            # print("sys.exc_info() : ", sys.exc_info())
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            print(str(e))
            logging.error("on_message {} {} {} \n {}".format(exc_type, fname, exc_tb.tb_lineno, str(e)))


def refresh_user_status():

    User.objects.\
        filter(is_bot=False,
               is_manager=False,
               is_staff=False).\
        update(is_online=False)


def authenticate_user(environ):

    header = environ.get('HTTP_AUTHORIZATION')
    if not header:
        return

    raw_token = header.split()[1]

    token = AccessToken(raw_token)

    try:
        # print(token.payload)
        user_id = token.get(django.conf.settings.SIMPLE_JWT['USER_ID_CLAIM'])
    except Exception as e:
        user_id = None
        print(e)

    return user_id


class UsersNamespace(socketio.AsyncNamespace):

    async def on_connect(self, sid, environ):

        user_id = authenticate_user(environ)
        # print('user_id', user_id)
        if not user_id:
            data = {"error_code": 401, "error": "not_authorized"}
            await self.emit('message', data, room=sid)
            return

        await self.save_session(sid, {'user_id': user_id})
        await redis.set(f'user_id{user_id}', sid)
        # print(f'{user_id} set in redis')

        await self.emit('success', 200, room=sid)

        User.objects.\
            filter(id=user_id).\
            update(is_online=True)

        # print(f'{user_id} CONNECTED')

    async def on_disconnect(self, sid):

        session = await self.get_session(sid)
        if 'user_id' not in session:
            data = {"error_code": 401, "error": "not_authorized"}
            await self.emit('message', data, room=sid)
            return

        user_id = session['user_id']
        User.objects.\
            filter(id=user_id).\
            update(is_online=False)

        await redis.delete(f'user_id{user_id}')

        # print(f'{user_id} DISCONNECTED')

    async def on_message(self, sid, data):

        try:

            # print('on_message')

            session = await self.get_session(sid)
            if 'user_id' not in session:
                data = {"error_code": 401, "error": "not_authorized"}
                await self.emit('message', data, room=sid)
                return

            from_user_id = data['from_user_id']
            to_user_id = data['to_user_id']
            text = data['text']

            user_id = session['user_id']
            if user_id != from_user_id:
                data = {"error_code": 400, "error": "no access"}
                await self.emit('message', data, room=sid)
                return

            from_user = User.objects.get(id=from_user_id)

            if from_user.is_block:
                data = {"error_code": 400, "error": "is_block"}
                await self.emit('message', data, room=sid)
                return

            to_user = User.objects.get(id=to_user_id)

            if 'image' in data:
                image = decode_base64_file(from_user_id, to_user_id, data['image'])
                # print(image)
            else:
                image = None

            if not from_user.is_premium:

                config = Config.get_solo()

                if Message.objects.filter(from_user=from_user, to_user=to_user).count() >= config.max_msg_per_chat:
                    data = {"error_code": 400, "error": "need_premium"}
                    await self.emit('message', data, room=sid)
                    return

                if Dialog.objects.filter(user=from_user).count() >= config.max_chats:
                    data = {"error_code": 400, "error": "need_premium"}
                    await self.emit('message', data, room=sid)
                    return

            m = Message.objects.create(from_user=from_user, to_user=to_user, text=text, image=image)

            try:
                from_dialog = Dialog.objects.get(user__id=from_user_id, peer_user__id=to_user_id)
                to_dialog = Dialog.objects.get(user__id=to_user_id, peer_user__id=from_user_id)

                from_dialog.last_message = m
                from_dialog.last_update_at = timezone.now()
                from_dialog.save(update_fields=['last_message', 'last_update_at'])

                to_dialog.unread_count += 1
                to_dialog.last_message = m
                to_dialog.last_update_at = timezone.now()
                to_dialog.save(update_fields=['last_message', 'unread_count', 'last_update_at'])

                is_first = False
            except Exception as e:
                print(e)
                from_dialog, to_dialog = Dialog.create_both_dialogs(from_user, to_user, m)
                is_first = True

            peer_dialog_serializer_from = DialogSerializer(instance=from_dialog)
            peer_dialog_serializer_to = DialogSerializer(instance=to_dialog)

            from_data = {
                "id": m.id,
                "from_user_id": from_user_id,
                "to_user_id": to_user_id,
                "text": text,
                "image": m.image.url if m.image else None,
                "peer_dialog": peer_dialog_serializer_to.data,
                "is_first": is_first,
                "created_at": localize_dt(m.created_at).strftime('%d.%m.%Y %H:%M:%S.%f')
            }

            to_data = {
                "id": m.id,
                "from_user_id": from_user_id,
                "to_user_id": to_user_id,
                "text": text,
                "image": m.image.url if m.image else None,
                "peer_dialog": peer_dialog_serializer_from.data,
                "is_first": is_first,
                "created_at": localize_dt(m.created_at).strftime('%d.%m.%Y %H:%M:%S.%f')
            }

            if to_user.is_bot:
                await sio.emit('message', from_data, room='managers', namespace='/managers')
            else:
                to_sid = await redis.get(f'user_id{to_user_id}', encoding='utf-8')
                if to_sid:
                    await self.emit('message', from_data, room=to_sid)

            await self.emit('message', to_data, room=sid)

            if to_user.device_token:
                asyncio.ensure_future(send_notification(to_user.device_token, from_user, to_user, m))

        except Exception as e:

            exc_type, exc_obj, exc_tb = sys.exc_info()
            # print("sys.exc_info() : ", sys.exc_info())
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            print(str(e))
            logging.error("on_message {} {} {} \n {}".format(exc_type, fname, exc_tb.tb_lineno, str(e)))
            data = {"error_code": 400, "error": "some error"}
            await self.emit('message', data, room=sid)

    async def on_mark_read(self, sid, data):

        session = await self.get_session(sid)
        if 'user_id' not in session:
            data = {"error_code": 401, "error": "not_authorized"}
            await self.emit('message', data, room=sid)
            return

        user_id = session['user_id']
        peer_user_id = data['peer_user_id']

        # user_id_ = session['user_id']
        # if user_id != user_id_:
        #     data = {"error_code": 400, "error": "no access"}
        #     await self.emit('message', data, room=sid)
        #     return

        try:
            d = Dialog.objects.get(user__id=user_id, peer_user__id=peer_user_id)
            d.unread_count = 0
            d.last_seen_at = timezone.now()
            d.save(update_fields=['unread_count', 'last_seen_at'])
        except:
            data = {"error_code": 400, "error": "dialog not found"}
            await self.emit('message', data, room=sid)
            return

        # await self.emit('message', "SUCCESS", room=sid)

        try:
            peer_user = User.objects.get(id=peer_user_id)
            peer_d = Dialog.objects.get(user__id=peer_user_id, peer_user__id=user_id)
        except Exception as e:
            print(e)
            return

        data = {
            'dialog_id': peer_d.id,
            'time':  localize_dt(timezone.now()).strftime('%d.%m.%Y %H:%M:%S.%f')
        }

        if peer_user.is_bot:
            await sio.emit('peer_mark_read', data, room='managers', namespace='/managers')
        else:
            to_sid = await redis.get(f'user_id{peer_user_id}', encoding='utf-8')
            if to_sid:
                await self.emit('peer_mark_read', data, room=to_sid)


sio.register_namespace(UsersNamespace('/users'))


class ManagerNamespace(socketio.AsyncNamespace):

    async def on_connect(self, sid, environ):

        # print('mangers connect')

        user_id = authenticate_user(environ)
        # print('user_id', user_id)
        if not user_id:
            data = {"error_code": 401, "error": "not_authorized"}
            await self.emit('message', data, room=sid)
            return

        user = User.objects.get(id=user_id)

        # print(user)

        if not user.is_manager:
            data = {"error_code": 401, "error": "not_manager"}
            await self.emit('message', data, room=sid)
            return

        await self.save_session(sid, {'manager_id': user_id})
        # await redis.set(f'manager_id{user_id}', sid)
        # print(f'{user_id} set in redis')

        # await self.server.enter_room(room, namespace=namespace or self.namespace)

        sio.enter_room(sid, 'managers', namespace='/managers')

        await self.emit('success', 200, room=sid)

        user.is_online = True
        user.save(update_fields=['is_online'])

        # User.objects.filter(is_bot=True).update(is_online=True)

        # print(f'{user_id} CONNECTED')

    async def on_disconnect(self, sid):

        session = await self.get_session(sid)
        if 'manager_id' not in session:
            data = {"error_code": 401, "error": "not_authorized"}
            await self.emit('message', data, room=sid)
            return

        user_id = session['manager_id']

        user = User.objects.get(id=user_id)

        if not user.is_manager:
            data = {"error_code": 401, "error": "not_manager"}
            await self.emit('message', data, room=sid)
            return

        sio.leave_room(sid, 'managers', namespace='/managers')

        user.is_online = False
        user.save(update_fields=['is_online'])

        await redis.delete(f'manager_id{user_id}')

        # User.objects.filter(is_bot=True).update(is_online=False)

        # print(f'{user_id} DISCONNECTED')

    # sio.emit('my reply', data, room='chat_users', skip_sid=sid)

    async def on_message(self, sid, data):

        try:

            # print("on_manager_message", data)

            session = await self.get_session(sid)
            if 'manager_id' not in session:
                data = {"error_code": 401, "error": "not_authorized"}
                await self.emit('message', data, room=sid)
                return

            from_user_id = data['from_user_id']
            to_user_id = data['to_user_id']
            text = data['text']

            from_user = User.objects.get(id=from_user_id)
            to_user = User.objects.get(id=to_user_id)

            if not from_user.is_bot:
                data = {"error_code": 401, "error": "from user is not bot"}
                await self.emit('message', data, room=sid)
                return

            if 'image' in data:
                image = decode_base64_file(from_user_id, to_user_id, data['image'])
            else:
                image = None

            m = Message.objects.create(from_user=from_user, to_user=to_user, text=text, image=image)

            try:
                from_dialog = Dialog.objects.get(user__id=from_user_id, peer_user__id=to_user_id)
                to_dialog = Dialog.objects.get(user__id=to_user_id, peer_user__id=from_user_id)

                from_dialog.last_message = m
                from_dialog.last_update_at = timezone.now()
                from_dialog.save(update_fields=['last_message', 'last_update_at'])

                to_dialog.unread_count += 1
                to_dialog.last_message = m
                to_dialog.last_update_at = timezone.now()
                to_dialog.save(update_fields=['last_message', 'unread_count', 'last_update_at'])

                is_first = False
            except Exception as e:
                print(e)
                from_dialog, to_dialog = Dialog.create_both_dialogs(from_user, to_user, m)
                is_first = True

            peer_dialog_serializer_from = DialogSerializer(instance=from_dialog)
            peer_dialog_serializer_to = DialogSerializer(instance=to_dialog)

            from_data = {
                "id": m.id,
                "from_user_id": from_user_id,
                "to_user_id": to_user_id,
                "text": text,
                "image": m.image.url if m.image else None,
                "peer_dialog": peer_dialog_serializer_to.data,
                "is_first": is_first,
                "created_at": localize_dt(m.created_at).strftime('%d.%m.%Y %H:%M:%S.%f')
            }

            to_data = {
                "id": m.id,
                "from_user_id": from_user_id,
                "to_user_id": to_user_id,
                "text": text,
                "image": m.image.url if m.image else None,
                "peer_dialog": peer_dialog_serializer_from.data,
                "is_first": is_first,
                "created_at": localize_dt(m.created_at).strftime('%d.%m.%Y %H:%M:%S.%f')
            }

            to_sid = await redis.get(f'user_id{to_user_id}', encoding='utf-8')

            if to_sid:
                await self.emit('message', from_data, room=to_sid, namespace='/users')
                # print(f'send message to {to_sid}')
            # await self.emit('message', to_data, room=sid)
            await sio.emit('message', to_data, room='managers', namespace='/managers')

            if to_user.device_token and not to_user.is_bot:
                asyncio.ensure_future(send_notification(to_user.device_token, from_user, to_user, m))

        except Exception as e:

            exc_type, exc_obj, exc_tb = sys.exc_info()
            # print("sys.exc_info() : ", sys.exc_info())
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            print(str(e))
            logging.error("on_message {} {} {} \n {}".format(exc_type, fname, exc_tb.tb_lineno, str(e)))
            data = {"error_code": 400, "error": "some error"}
            await self.emit('message', data, room=sid)

    async def on_mark_read(self, sid, data):

        # print(data)

        session = await self.get_session(sid)
        if 'manager_id' not in session:
            data = {"error_code": 401, "error": "not_authorized"}
            await self.emit('message', data, room=sid)
            return

        bot_user_id = data['bot_user_id']
        peer_user_id = data['peer_user_id']

        # print(bot_user_id, peer_user_id)

        from_user = User.objects.get(id=bot_user_id)
        # to_user = User.objects.get(id=to_user_id)

        if not from_user.is_bot:
            data = {"error_code": 401, "error": "from user is not bot"}
            await self.emit('message', data, room=sid)
            return

        # user_id_ = session['user_id']
        # if user_id != user_id_:
        #     data = {"error_code": 400, "error": "no access"}
        #     await self.emit('message', data, room=sid)
        #     return

        try:
            d = Dialog.objects.get(user__id=bot_user_id, peer_user__id=peer_user_id)
            d.unread_count = 0
            d.last_seen_at = timezone.now()
            d.save(update_fields=['unread_count', 'last_seen_at'])
        except:
            data = {"error_code": 400, "error": "dialog not found"}
            await self.emit('message', data, room=sid)
            return

        # await self.emit('message', "SUCCESS", room=sid)

        try:
            # peer_user = User.objects.get(id=peer_user_id)
            peer_d = Dialog.objects.get(user__id=peer_user_id, peer_user__id=bot_user_id)
        except Exception as e:
            print(e)
            return

        data = {
            'dialog_id': peer_d.id,
            'time':  localize_dt(timezone.now()).strftime('%d.%m.%Y %H:%M:%S.%f')
        }

        # if peer_user.is_bot:
        #     await sio.emit('peer_mark_read', data, room='managers', namespace='/managers')
        # else:

        to_sid = await redis.get(f'user_id{peer_user_id}', encoding='utf-8')
        if to_sid:
            # print('test2222222222222222222222')
            await self.emit('peer_mark_read', data, room=to_sid, namespace='/users')

sio.register_namespace(ManagerNamespace('/managers'))



if __name__ == '__main__':
    # sio.start_background_task(background_task)
    loop = asyncio.get_event_loop()

    refresh_user_status()

    loop.run_until_complete(init_redis())
    web.run_app(app, port=8001)




# @sio.event
# async def connect(sid, environ):
#
#     user_id = sync_to_async(authenticate_user(environ))
#     if no
#     await sio.save_session(sid, {'user_id': user_id})
#     session = await sio.get_session(sid)
#     print(session)
#     print(session['user'])
#     print(session['user'].id)
#     print(session['user'].phone)
#
#     await sio.emit('news', "", room=sid)
#
#
# @sio.event
# def message(sid, data):
#     session = sio.get_session(sid)
#     print('message from ', session['username'])
#
#
# @sio.event
# def disconnect(sid):
#     print('Client disconnected')



# @sio.event(namespace='/news')
# async def background_task():
#     """Example of how to send server generated events to clients."""
#     count = 0
#     while True:
#         await sio.sleep(15)
#         print('news')
#         count += 1
#         await sio.emit('news', "Новая статья")
#
#
# async def index(request):
#     with open('app.html') as f:
#         return web.Response(text=f.read(), content_type='text/html')
#
#
# @sio.event(namespace='/news')
# async def my_event(sid, message):
#     print('my_event')
#     print(sid, message)
#     await sio.emit('news', "Новая статья", room=sid)
#
#
# @sio.event(namespace='/news')
# async def my_event2(sid, message):
#     print('my_event2')
#     print(sid, message)
#     await sio.emit('news', {'data': 'Новая статья'}, room=sid)
#
#
# @sio.event(namespace='/news')
# async def publish(sid, message):
#     print('publish')
#     print(sid, message)
#     await sio.emit('news', "publish", room=sid)
#
#
# @sio.event(namespace='/news')
# async def publish2(sid, message):
#     print('publish2')
#     print(sid, message)
#     await sio.emit('news', "publish2", room=sid)
#
#
# @sio.event
# async def my_broadcast_event(sid, message):
#     print(sid, message)
#     await sio.emit('news', "")
#
#
# @sio.event
# async def join(sid, message):
#     print(sid, message)
#     sio.enter_room(sid, message['room'])
#     await sio.emit('news', "",
#                    room=sid)
#
#
# @sio.event
# async def leave(sid, message):
#     print(sid, message)
#     sio.leave_room(sid, message['room'])
#     await sio.emit('news', "",
#                    room=sid)
#
#
# @sio.event
# async def close_room(sid, message):
#     print(sid, message)
#     await sio.emit('news',
#                    "",
#                    room=message['room'])
#     await sio.close_room(message['room'])
#
#
# @sio.event
# async def my_room_event(sid, message):
#     print(sid, message)
#     await sio.emit('news', "",
#                    room=message['room'])
#
#
# @sio.event
# async def disconnect_request(sid):
#     print(sid)
#     await sio.disconnect(sid)
#
#
# @sio.event
# def connect2(sid, environ):
#     username = authenticate_user(environ)
#     sio.save_session(sid, {'username': username})
