import asyncpg
from datetime import datetime
pool = None
from decouple import config
import json


async def init_db():
    global pool
    pool = await asyncpg.create_pool(f"postgresql://{config('POSTGRES_USER', default='postgres')}:{config('POSTGRES_PASSWORD', default='')}@{config('POSTGRES_HOST', default='db')}:{config('POSTGRES_PORT', default=5432, cast=int)}/{config('POSTGRES_DB', default='postgres')}")
    # await create_tables()


async def add_user(chat_id, username, fio, parent, date_in):
    async with pool.acquire() as conn:

        query = "INSERT INTO bot_botuser " \
                "(chat_id, username, fio, step, " \
                "lang, parent, otpiska, date_in) " \
                "VALUES " \
                "($1::bigint, $2::varchar, $3::varchar, 'home', " \
                "NULL, $4::bigint, FALSE, $5::timestamp) " \
                "RETURNING *"
        result = await conn.fetch(query, chat_id, username, fio, parent, date_in)

        if not result:
            return []
        else:
            return result[0]


async def update_user(chat_id, **kwargs):

    if not kwargs:
        return

    sql_str = ''
    var_number = 1
    vars = []

    for key in kwargs:

        sql_str += key + "=$%d," % var_number
        var_number += 1
        vars.append(kwargs[key])

    sql_query = "UPDATE bot_botuser SET %s WHERE chat_id = $%d" % (sql_str[0:-1], var_number)
    vars.append(chat_id)

    async with pool.acquire() as conn:
        return await conn.fetch(sql_query, *vars)


async def get_user(chat_id):
    async with pool.acquire() as conn:
        result = await conn.fetch('SELECT * FROM bot_botuser WHERE chat_id = %s' % chat_id)
        if not result:
            return []
        else:
            return result[0]


async def get_refcount(chat_id):
    async with pool.acquire() as conn:
        result = await conn.fetch('SELECT count(*) FROM bot_botuser WHERE parent = %s' % chat_id)
        return result[0]["count"]


async def get_user_by_id(id):
    async with pool.acquire() as conn:
        result = await conn.fetch('SELECT * FROM bot_botuser WHERE id = %s' % id)
        if not result:
            return []
        else:
            return result[0]


async def get_kurs():
    async with pool.acquire() as conn:
        result = await conn.fetch('SELECT * FROM bot_settings')
        if not result:
            return []
        else:
            return result[0]["kurs"]


async def get_settings():
    async with pool.acquire() as conn:
        result = await conn.fetch('SELECT * FROM bot_settings LIMIT 1')
        if not result:
            return []
        else:
            return result[0]


async def get_count_users():
    async with pool.acquire() as conn:
        result = await conn.fetch('SELECT count(*) FROM bot_botuser')
        if not result:
            return []
        else:
            return result[0]["count"]


async def get_summa():
    async with pool.acquire() as conn:
        result = await conn.fetch('SELECT sum(balance) FROM bot_botuser')
        if not result:
            return []
        else:
            return result[0]["sum"]


async def get_count_today_users(date):
    async with pool.acquire() as conn:
        result = await conn.fetch("SELECT count(*) FROM bot_botuser where DATE(date_in + interval '3 hours') = '%s'" % date)
        if not result:
            return []
        else:
            return result[0]["count"]


async def get_users(offset, limit):
    print('get_users %s %s' % (offset, limit))

    with open("error_compaign.txt", 'a+') as file:
        file.write('get_users %s %s\n' % (offset, limit))

    async with pool.acquire() as conn:
        return await conn.fetch("SELECT * FROM bot_botuser WHERE otpiska = FALSE ORDER BY id ASC OFFSET %s LIMIT %s" % (offset, limit))
        #return await conn.fetch("SELECT * FROM bot_botuser ORDER BY id ASC OFFSET %s LIMIT %s" % (offset, limit))


async def update_rate(rate_id, **kwargs):

    if not kwargs:
        return

    sql_str = ''
    var_number = 1
    vars = []

    for key in kwargs:

        sql_str += key + "=$%d," % var_number
        var_number += 1
        vars.append(kwargs[key])

    sql_query = "UPDATE bot_rate SET %s WHERE id = $%d" % (sql_str[0:-1], var_number)
    vars.append(rate_id)

    async with pool.acquire() as conn:
        return await conn.fetch(sql_query, *vars)


async def get_rate_by_name(name):

    async with pool.acquire() as conn:

        result = await conn.fetch('SELECT * FROM bot_rate WHERE name = $1', name)
        if not result:
            return []
        else:
            return result[0]


async def get_orders_for_cancel():
     async with pool.acquire() as conn:
        return await conn.fetch("SELECT * "
                                "FROM bot_order "
                                "WHERE date_cancel < $1 AND "
                                "status IN ('created', 'wait_confs')", datetime.today())


async def get_orders_for_check_payments():
    async with pool.acquire() as conn:
        return await conn.fetch("SELECT * "
                                "FROM bot_order "
                                "WHERE i_payed_dt IS NOT NULL AND "
                                "status = 'created'")


async def update_order(order_id, **kwargs):

    if not kwargs:
        return

    sql_str = ''
    var_number = 1
    vars = []

    for key in kwargs:

        sql_str += key + "=$%d," % var_number
        var_number += 1
        vars.append(kwargs[key])

    sql_query = "UPDATE bot_order SET %s WHERE id = $%d" % (sql_str[0:-1], var_number)
    vars.append(order_id)

    async with pool.acquire() as conn:
        return await conn.fetch(sql_query, *vars)
